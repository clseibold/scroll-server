package main

import (
	"bufio"
	"io"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func MimetypeGetsLangParam(mt_string string) bool {
	return mt_string == "text/plain" || strings.HasPrefix(mt_string, "text/") || mt_string == "text/gemini" || mt_string == "text/gophermap" || mt_string == "text/nex" || mt_string == "text/scroll" || mt_string == "text/html" || mt_string == "application/xhtml+xml" || mt_string == "application/xml" || mt_string == "application/mbox" || strings.HasPrefix(mt_string, "multipart/") || mt_string == "message/rfc822"
}

func MimetypeToGopherItemtype(mt_string string, mt_parent_string string) rune {
	if mt_parent_string == "" {
		mt_parent_string = mt_string
	}

	fileItemType := '0'
	if mt_parent_string == "text/plain" || strings.HasPrefix(mt_string, "text/") || mt_string == "application/xml" || mt_string == "application/base64" || mt_string == "text/x-uuencode" {
		fileItemType = '0' // Text file
	} else if mt_string == "text/gophermap" || mt_string == "text/gemini" || mt_string == "text/nex" {
		fileItemType = '0' // Directory listings
	} else if CertMimetype(mt_string) || mt_string == "application/x-msdownload" || mt_string == "application/octet-stream" || mt_string == "application/x-executable" || mt_string == "application/vnd.microsoft.portable-executable" {
		fileItemType = '9'
	} else if mt_string == "text/html" || mt_parent_string == "text/html" || mt_string == "application/xhtml+xml" {
		fileItemType = 'h'
	} else if ArchiveMimetype(mt_string) {
		fileItemType = '5' // Archive
	} else if mt_string == "image/gif" {
		fileItemType = 'g'
	} else if strings.HasPrefix(mt_string, "image/") {
		fileItemType = 'I'
	} else if DocumentMimetype(mt_string) {
		fileItemType = 'd'
	} else if AudioMimetype(mt_string) {
		fileItemType = 's'
	} else if VideoMimetype(mt_string) {
		fileItemType = ';'
	} else if CalendarMimetype(mt_string) { // TODO
		fileItemType = 'c'
	} else if mt_string == "application/mbox" || strings.HasPrefix(mt_string, "multipart/") || mt_string == "message/rfc822" {
		fileItemType = 'M'
	} else { // Assume binary
		fileItemType = '9'
	}

	return fileItemType
}

// Use when you cannot get the file's mimetype, like if converting URLs
func FilenameToGopherItemtype(filename string) rune {
	//fileItemType := '0'
	extension := filepath.Ext(filename)
	switch extension {
	case ".mp3", ".ogg":
		return '9'
	case ".mp4":
		return '9'
	default:
		return '0'
	}
}

func CertFilename(filename string) bool {
	extension := filepath.Ext(filename)
	switch extension {
	case ".pem", ".csr", ".crt", ".cert", ".cer", ".key", ".der", ".pkcs12", ".pfx", ".p12", ".p7b", ".keystore", ".crl", ".prf", ".p7r", ".spp", ".spq", ".scs", ".scq", ".pkipath", ".pki", ".ac", ".p8", ".p7s", ".p10":
		return true
	case ".pgp": // pgp files are allowed
		return false
	}

	return false
}
func CertMimetype(mimetype string) bool {
	switch mimetype {
	case "application/x-pem-file", "application/pkcs7-mime", "application/pics-rules", "application/x-x509-ca-cert", "application/x-pkcs7-certreqresp", "application/x-pkcs7-certificates", "application/x-pkcs12", "application/scvp-vp-response", "application/scvp-vp-request", "application/scvp-cv-response", "application/scvp-cv-request", "application/pkix-pkipath", "application/pkix-crl", "application/pkixcmp", "application/pkix-cert", "application/pkix-attr-cert", "application/pkcs8", "application/pkcs7-signature", "application/pkcs10":
		return true
	case "application/pgp-signature", "application/pgp-encrypted":
		return false
	}

	return false
}
func PrivacySensitiveFilename(filename string) bool {
	extension := filepath.Ext(filename)
	switch extension {
	case ".pem", ".csr", ".crt", ".cert", ".cer", ".key", ".der", ".pkcs12", ".pfx", ".p12", ".p7b", ".keystore", ".crl", ".prf", ".p7r", ".spp", ".spq", ".scs", ".scq", ".pkipath", ".pki", ".ac", ".p8", ".p7s", ".p10":
		return true
	case ".pgp": // pgp files are allowed
		return false
	}

	base := filepath.Base(filename)
	if base == "sis.conf" || base == "sis.exe" || base == "sis" {
		return true
	}

	return false
}
func PrivacySensitiveMimetype(mimetype string) bool {
	switch mimetype {
	case "application/x-pem-file", "application/pkcs7-mime", "application/pics-rules", "application/x-x509-ca-cert", "application/x-pkcs7-certreqresp", "application/x-pkcs7-certificates", "application/x-pkcs12", "application/scvp-vp-response", "application/scvp-vp-request", "application/scvp-cv-response", "application/scvp-cv-request", "application/pkix-pkipath", "application/pkix-crl", "application/pkixcmp", "application/pkix-cert", "application/pkix-attr-cert", "application/pkcs8", "application/pkcs7-signature", "application/pkcs10":
		return true
	case "application/pgp-signature", "application/pgp-encrypted":
		return false
	}

	return false
}
func DotFilename(filename string) bool {
	base := filepath.Base(filename)
	return strings.HasPrefix(base, ".")
}

func ArchiveMimetype(mimetype string) bool {
	switch mimetype {
	case "application/x-stuffit", "application/vnd.airzip.filesecure.azf", "application/vnd.airzip.filesecure.azs", "application/vnd.android.package-archive", "application/vnd.joost.joda-archive", "application/vnd.ms-cab-compressed", "application/x-7z-compressed", "application/x-stuffitx", "application/zip", "application/x-ace-compressed", "application/x-apple-diskimage", "application/x-bcpio", "application/x-bzip", "application/x-bzip2", "application/x-debian-package", "application/x-gtar", "application/x-rar-compressed", "application/x-shar", "application/x-ustar", "application/x-gzip", "application/x-tar", "application/tar", "application/x-compress", "application/x-compressed", "application/gzip", "application/x-tgz":
		return true
	case "application/java-archive":
		return true
	}

	return false
}

// Where do markup language files get placed?
func DocumentMimetype(mimetype string) bool {
	switch mimetype {
	case "application/pdf": // Adobe (PDF and PostScript)
		return true
	case "application/vnd.amazon.ebook", "application/epub+zip": // Ebooks
		return false
	case "application/vnd.google-apps.document": // Google Docs/Apps
		return true
	case "application/vnd.lotus-notes", "application/vnd.lotus-wordpro": // Lotus
		return true
	case "application/vnd.kde.kword": // KDE
		return true
	case "application/x-mswrite", "application/msword", "application/vnd.ms-works", "application/vnd.ms-word.document.macroenabled.12", "application/vnd.openxmlformats-officedocument.wordprocessingml.document": // MS Office
		return true
	case "application/vnd.oasis.opendocument.text", "application/vnd.oasis.opendocument.text-master", "application/vnd.oasis.opendocument.text-web", "application/vnd.sun.xml.writer.global", "application/vnd.sun.xml.writer": // Open Document
		return true
	case "application/vnd.stardivision.writer-global", "application/vnd.stardivision.writer": // Star Office
		return true
	case "application/vnd.wordperfect": // WordPerfect
		return true
	case "application/postscript":
		return true
	}
	return false
}
func CalendarMimetype(mimetype string) bool {
	switch mimetype {
	case "text/x-vcalendar", "text/calendar":
		return true
	}
	return false
}
func AudioMimetype(mimetype string) bool {
	if strings.HasPrefix(mimetype, "audio/") {
		return true
	}
	switch mimetype {
	case "application/vnd.epson.msf", "application/vnd.yamaha.smaf-phrase", "application/vnd.yamaha.smaf-audio", "application/vnd.yamaha.openscoreformat.osfpvg+xml", "application/vnd.yamaha.openscoreformat", "application/vnd.yamaha.hv-voice", "application/vnd.yamaha.hv-script", "application/vnd.yamaha.hv-dic", "application/vnd.epson.esf", "application/vnd.epson.ssf":
		return true
	case "application/vnd.google-apps.audio": // Google Apps Audio
		return true
	}
	return false
}
func VideoMimetype(mimetype string) bool {
	if strings.HasPrefix(mimetype, "video/") {
		return true
	}
	switch mimetype {
	case "application/mp4", "application/mp21":
		return true
	case "application/x-cdlink", "application/x-dvi", "application/ogg": // TODO: ogg can be video or audio
		return true
	case "application/vnd.epson.quickanime", "application/vnd.mseq", "application/vnd.rn-realmedia", "application/vnd.epson.salt", "application/vnd.google-apps.video", "application/x-director":
		return true
	}
	return false
}

// Scans until it sees a level-1 heading, and then stops scanning and returns it
// Works with gemtext, scrolltext, and markdown
func GetInlineMetadataFromGemtext(reader io.Reader) (title string, author string, publishdate time.Time, modificationdate time.Time, language string, classification ScrollResponseUDC) {
	title = ""

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "# ") {
			title = strings.TrimSpace(line)
			break
		} else {
			field, value, hasValue := strings.Cut(line, ":")
			field = strings.ToLower(strings.TrimSpace(field))
			value = strings.TrimSpace(value)
			if hasValue {
				if field == "author" {
					author = value
				} else if field == "publish-date" {
					parsed_publishdate, err := time.Parse(time.RFC3339, string(value))
					if err == nil {
						publishdate = parsed_publishdate
					}
				} else if field == "modification-date" || field == "modified-date" || field == "update-date" || field == "updated-date" {
					parsed_modificationdate, err := time.Parse(time.RFC3339, string(value))
					if err == nil {
						modificationdate = parsed_modificationdate
					}
				} else if field == "language" {
					language = value
				} else if field == "udc-class" {
					// Check if integer first, if not, then parse as string
					c, err := strconv.Atoi(value)
					classification = ScrollResponseUDC(c)
					if err != nil {
						classification = ClassificationStringToUDC(value)
					}
				}
			}
		}
	}

	return title, author, publishdate, modificationdate, language, classification
}

// Returns the header data (everything up to but excluding the first title of the document)
func ScanUpToTitle(reader io.ReadSeeker) string {
	headerDataBuilder := strings.Builder{}
	newLine := true
	buffer := make([]byte, 1)
	for {
		n, err := reader.Read(buffer)
		if err == io.EOF && n <= 0 {
			break
		} else if err != nil {
			break
		} else if buffer[0] == '\n' {
			newLine = true
			headerDataBuilder.WriteByte(buffer[0])
		} else if newLine && buffer[0] == '#' {
			reader.Seek(-1, io.SeekCurrent)
			break
		} else {
			newLine = false
			headerDataBuilder.WriteByte(buffer[0])
		}
	}

	return headerDataBuilder.String()
}
