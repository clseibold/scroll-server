package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func (server *Server) loadConfig() {
	if server.Hosts == nil {
		server.Hosts = make(map[string]*Host, 1)
	}

	file, err := os.OpenFile(filepath.Join(server.ServerDirectory, "scrollserver.conf"), os.O_RDONLY, 0600)
	if err != nil {
		fmt.Printf("Error: Couldn't open config file.\n")
		return
	}

	currentSectionName := ""
	currentHost := NewHost(server)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		scanned_text := scanner.Text()
		line := strings.TrimRight(scanned_text, "\r\n")

		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") {
			// Deal with previous section
			if currentSectionName != "scroll-server" {
				currentHost.Hostname = currentSectionName
				if currentHost.Default {
					server.DefaultHostname = currentHost.Hostname
				}
				if currentHost.CertificateFile == "" {
					currentHost.CertificateFile = filepath.Join(server.ServerDirectory, currentHost.Hostname+".pem")
				}
				if currentHost.Directory == "" {
					currentHost.Directory = filepath.Join(server.ServerDirectory, currentHost.Hostname)
					os.Mkdir(currentHost.Directory, 0700)
				}
				server.Hosts[currentSectionName] = currentHost
			}

			// Start of new section
			currentSectionName = strings.TrimSuffix(strings.TrimPrefix(line, "["), "]")
			currentHost = NewHost(server)
		} else {
			field, value, hasValue := strings.Cut(line, ":")
			field = strings.TrimSpace(field)
			value = strings.TrimSpace(value)
			if !hasValue {
				continue
			}

			if currentSectionName == "scroll-server" { // Handle default section
				switch field {
				case "bind-address":
					server.BindAddress = value
				case "port":
					server.Port = value
				case "serve-port":
					server.ServePort = value
				case "rate-limit":
					integer, err := strconv.Atoi(value)
					if err != nil {
						fmt.Printf("Warning: Couldn't parse integer of rate-limit field.\n")
						continue
					}
					server.RateLimit = integer
				case "max-concurrent-connections":
					integer, err := strconv.Atoi(value)
					if err != nil {
						fmt.Printf("Warning: Couldn't parse integer of rate-limit field.\n")
						continue
					}
					server.MaxConcurrentConnections = integer
				case "default-language":
					server.DefaultLanguage = value
				}
			} else {
				switch field {
				case "certificate":
					currentHost.CertificateFile, _ = filepath.Abs(value)
				case "default":
					if value == "true" {
						currentHost.Default = true
					}
				}
			}
		}
	}

	// Deal with last Host
	if currentSectionName != "" {
		if currentSectionName != "scroll-server" {
			currentHost.Hostname = currentSectionName
			if currentHost.Default {
				server.DefaultHostname = currentHost.Hostname
			}
			if currentHost.CertificateFile == "" {
				currentHost.CertificateFile = filepath.Join(server.ServerDirectory, currentHost.Hostname+".pem")
			}
			if currentHost.Directory == "" {
				currentHost.Directory = filepath.Join(server.ServerDirectory, currentHost.Hostname)
				os.Mkdir(currentHost.Directory, 0700)
			}
			server.Hosts[currentSectionName] = currentHost
		}
	}

	if server.ServePort == "" {
		server.ServePort = server.Port
	}
	if server.MaxConcurrentConnections == 0 {
		server.MaxConcurrentConnections = 2000
	}
}

func (server *Server) saveConfig() {
	var builder strings.Builder

	fmt.Fprintf(&builder, "[scroll-server]\n")
	fmt.Fprintf(&builder, "bind-address: %s\n", server.BindAddress)
	fmt.Fprintf(&builder, "port: %s\n", server.Port)
	fmt.Fprintf(&builder, "serve-port: %s\n", server.Port)
	fmt.Fprintf(&builder, "rate-limit: %d\n", server.RateLimit)
	fmt.Fprintf(&builder, "max-concurrent-connections: %d\n", server.MaxConcurrentConnections)
	if server.DefaultLanguage != "" {
		fmt.Fprintf(&builder, "default-language: %s\n", server.DefaultLanguage)
	}

	for _, host := range server.Hosts {
		fmt.Fprintf(&builder, "\n[%s]\n", host.Hostname)
		fmt.Fprintf(&builder, "certificate: %s\n", host.CertificateFile)
		if host.Default {
			fmt.Fprintf(&builder, "default: true\n")
		}
	}

	os.WriteFile(filepath.Join(server.ServerDirectory, "scrollserver.conf"), []byte(builder.String()), 0600)
}
