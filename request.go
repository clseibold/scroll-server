package main

import (
	"bufio"
	"crypto/md5"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"fmt"
	"io"
	"mime"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	_ "time/tzdata"

	"github.com/dhowden/tag"
	"github.com/djherbis/times"
	"github.com/gabriel-vasile/mimetype"
	"golang.org/x/text/language"
)

type ScrollMetadata struct {
	Author         string
	PublishDate    time.Time // Should be in UTC
	UpdateDate     time.Time // Should be in UTC
	Language       string    // Should use BCP47
	Abstract       string
	Classification ScrollResponseUDC
}

type ScrollResponseUDC uint8

const (
	ScrollResponseUDC_Class4 ScrollResponseUDC = iota
	ScrollResponseUDC_Class0                   // Knowledge, General Science, Docs, RFCs, Software, Data
	ScrollResponseUDC_Class1
	ScrollResponseUDC_Class2
	ScrollResponseUDC_Class3
	ScrollResponseUDC_Class5
	ScrollResponseUDC_Class6
	ScrollResponseUDC_Class7
	ScrollResponseUDC_Class8
	ScrollResponseUDC_Class9

	ScrollResponseUDC_GeneralKnowledge   = ScrollResponseUDC_Class0
	ScrollResponseUDC_Docs               = ScrollResponseUDC_Class0
	ScrollResponseUDC_Data               = ScrollResponseUDC_Class0
	ScrollResponseUDC_GeneralScience     = ScrollResponseUDC_Class0
	ScrollResponseUDC_Reference          = ScrollResponseUDC_Class0
	ScrollResponseUDC_CompSci            = ScrollResponseUDC_Class0
	ScrollResponseUDC_News               = ScrollResponseUDC_Class0
	ScrollResponseUDC_ComputerTechnology = ScrollResponseUDC_Class0

	ScrollResponseUDC_Philosophy = ScrollResponseUDC_Class1
	ScrollResponseUDC_Psychology = ScrollResponseUDC_Class1

	ScrollResponseUDC_Religion  = ScrollResponseUDC_Class2
	ScrollResponseUDC_Theology  = ScrollResponseUDC_Class2
	ScrollResponseUDC_Scripture = ScrollResponseUDC_Class2

	ScrollResponseUDC_SocialScience = ScrollResponseUDC_Class3
	ScrollResponseUDC_Politics      = ScrollResponseUDC_Class3

	ScrollResponseUDC_Unclassed  = ScrollResponseUDC_Class4
	ScrollResponseUDC_Aggregator = ScrollResponseUDC_Class4
	ScrollResponseUDC_Directory  = ScrollResponseUDC_Class4
	ScrollResponseUDC_Index_Menu = ScrollResponseUDC_Class4
	ScrollResponseUDC_Misc       = ScrollResponseUDC_Class4

	ScrollResponseUDC_Mathematics    = ScrollResponseUDC_Class5
	ScrollResponseUDC_NaturalScience = ScrollResponseUDC_Class5

	ScrollResponseUDC_AppliedScience    = ScrollResponseUDC_Class6
	ScrollResponseUDC_Medicine          = ScrollResponseUDC_Class6
	ScrollResponseUDC_Health            = ScrollResponseUDC_Class6
	ScrollResponseUDC_GeneralTechnology = ScrollResponseUDC_Class6
	ScrollResponseUDC_Engineering       = ScrollResponseUDC_Class6
	ScrollResponseUDC_Business          = ScrollResponseUDC_Class6
	ScrollResponseUDC_Accountancy       = ScrollResponseUDC_Class6

	ScrollResponseUDC_Art                = ScrollResponseUDC_Class7
	ScrollResponseUDC_Fashion            = ScrollResponseUDC_Class7
	ScrollResponseUDC_Entertainment      = ScrollResponseUDC_Class7
	ScrollResponseUDC_Music              = ScrollResponseUDC_Class7
	ScrollResponseUDC_GamingVideos       = ScrollResponseUDC_Class7
	ScrollResponseUDC_FictionalMovies    = ScrollResponseUDC_Class7
	ScrollResponseUDC_FictionalAnimation = ScrollResponseUDC_Class7
	ScrollResponseUDC_FictionalTVShows   = ScrollResponseUDC_Class7
	ScrollResponseUDC_Sport              = ScrollResponseUDC_Class7
	ScrollResponseUDC_Fitness            = ScrollResponseUDC_Class7
	ScrollResponseUDC_Recreation         = ScrollResponseUDC_Class7

	ScrollResponseUDC_Linguistics       = ScrollResponseUDC_Class8
	ScrollResponseUDC_Literature        = ScrollResponseUDC_Class8
	ScrollResponseUDC_LiteraryCriticism = ScrollResponseUDC_Class8
	ScrollResponseUDC_Memoir            = ScrollResponseUDC_Class8
	ScrollResponseUDC_PersonalLog       = ScrollResponseUDC_Class8
	ScrollResponseUDC_BookReview        = ScrollResponseUDC_Class8
	ScrollResponseUDC_MovieReview       = ScrollResponseUDC_Class8
	ScrollResponseUDC_VideoReview       = ScrollResponseUDC_Class8
	ScrollResponseUDC_MusicReview       = ScrollResponseUDC_Class8

	ScrollResponseUDC_History   = ScrollResponseUDC_Class9
	ScrollResponseUDC_Geography = ScrollResponseUDC_Class9
	ScrollResponseUDC_Biography = ScrollResponseUDC_Class9
)

func UDCToSuccessResponse(value ScrollResponseUDC) int {
	switch value {
	case ScrollResponseUDC_Class4:
		return 24
	case ScrollResponseUDC_Class0:
		return 20
	case ScrollResponseUDC_Class1:
		return 21
	case ScrollResponseUDC_Class2:
		return 22
	case ScrollResponseUDC_Class3:
		return 23
	case ScrollResponseUDC_Class5:
		return 25
	case ScrollResponseUDC_Class6:
		return 26
	case ScrollResponseUDC_Class7:
		return 27
	case ScrollResponseUDC_Class8:
		return 28
	case ScrollResponseUDC_Class9:
		return 29
	default:
		return 24
	}
}

func ClassificationStringToUDC(value string) ScrollResponseUDC {
	switch strings.ToLower(value) {
	case "knowledge", "docs", "documentation", "data", "general science", "general knowledge", "reference", "computer science", "news", "software repository", "software repo", "computer technology", "howto", "tutorial", "devlog":
		return ScrollResponseUDC_Class0
	case "philosophy", "psychology":
		return ScrollResponseUDC_Class1
	case "religion", "theology", "scripture":
		return ScrollResponseUDC_Class2
	case "social science", "social sciences", "law", "politics", "gender studies", "sociology", "LGBTQ+", "military", "military affairs", "education":
		return ScrollResponseUDC_Class3
	case "mathematics", "math", "maths", "natural science":
		return ScrollResponseUDC_Class5
	case "applied science", "applied sciences", "technology", "tech", "medicine", "health", "engineering", "business", "accountancy", "accounting":
		return ScrollResponseUDC_Class6
	case "art", "arts", "entertainment", "music", "gaming", "gaming video", "gaming videos", "sport", "sports", "fictional movies", "fictional movie", "fictional animation", "fictional animations", "fictional anime", "fictional animes", "fashion", "beauty", "culinary art", "culinary arts", "recreation", "fitness", "drama", "performance", "performance art", "performance arts":
		return ScrollResponseUDC_Class7
	case "linguistics", "language", "langauges", "literature", "literary criticism", "biblical criticism", "memoir", "personal log", "personal blog", "book review", "movie review", "video review", "music review":
		return ScrollResponseUDC_Class8
	case "history", "geography", "biography", "autobiography":
		return ScrollResponseUDC_Class9
	case "unclassed", "unclassed software", "general software", "general aggregator", "general directory", "general index", "menu":
		return ScrollResponseUDC_Class4
	default:
		return ScrollResponseUDC_Class4
	}
}

type Request struct {
	Protocol      ProtocolType
	Upload        bool              // true if request supports reading uploaded data
	fromRoute     string            // What route the request came from
	requestString string            // Path for Nex, URL for Gopher, Gemini, and Misfin. Always excludes the query
	_rawQuery     string            // The raw query (always escaped, without '?', or '\t' for gopher). Spartan does not use this, but uses the Data field instead.
	_queryLimit   int64             // Used by Spartan servers to limit data for queries. If 0, use the default of 1024.
	GlobString    string            // Set to the path components of the glob part of the route
	readwriter    io.ReadWriter     // Reads from and Writes to the connection (net or tls)
	params        map[string]string // Stores route params and their values

	// Upload Data
	DataSize int64  // Expected Size for upload (-1 for any size until connection close)
	_data    []byte // Used to cache raw uploaded data. For NPS, the data is already read before calling the handler. If proxying from Spartan servers, the data is set to a query string, if used.
	DataMime string // Used for Titan and other protocols that support upload with mimetype. For Spartan, it is usually empty or "text/plain" if proxying to Spartan servers from servers that use queries.

	// More Request Data
	UserCert *x509.Certificate // Used for protocols that support TLS Client Certs (Misfin and Gemini)
	IP       string

	// Response Data
	servePath   string // Used internally to serve static files/directories.
	headerSent  bool   // Stores whether the response header has already been sent (for protocols that have this)
	convertMode bool   // When turned on, converts from given format to default format of protocol

	// Proxying Data
	proxyRoute       string // Used to proxy routes from one server to another, or within the same server.
	proxiedUnder     string // The path being proxied under. This is necessary to proxy gemini and nex over to gopher, because gopher requires links be absolute to originating server.
	proxiedFromRoute string // The route being proxied from
	//origRequest  string // Gives you the original request string. Used for proxying.

	// More Response Data
	allowServeCert     bool // Allows you to serve a cert/key file as the request. Will be set to the route's security setting.
	allowServeDotFiles bool // Allows you to serve dot (and hidden) files as the request. Will be set to the route's security setting.

	// TODO: Add childrenRoutes and gopherItemType fields
	gopherItemType rune // Set to whatever the RouteNode was set to

	Proxied bool
	Host    *Host
	Server  *Server

	// Used for Scroll Protocol
	ScrollMetadataRequested  bool
	ScrollRequestedLanguages []string
	scrollMetadata           ScrollMetadata
}

// Used for all protocols that send a mimetype in the response
func (r *Request) SetLanguage(lang string) {
	r.scrollMetadata.Language = lang
}

// Forces the response to have no lang parameter
func (r *Request) SetNoLanguage() {
	r.scrollMetadata.Language = "none"
}

func (r *Request) SetScrollMetadataResponse(metadata ScrollMetadata) {
	r.scrollMetadata = metadata
}
func (r *Request) ClearScrollMetadataResponse() {
	r.scrollMetadata = ScrollMetadata{}
}

// Get Titan or Spartan raw data.
func (r *Request) GetUploadData() ([]byte, error) { // TODO: Return error when there's no data received after a timeout.
	if !r.Upload {
		return []byte{}, fmt.Errorf("current page does not allow upload")
	}

	return r._getUploadData()
}

func (r *Request) _getUploadData() ([]byte, error) {
	// If data has already been read, then return that. This is useful for multiple calls of this method, and for proxying to Spartan servers (where query strings get set to the data).
	if len(r._data) > 0 {
		return r._data, nil
	}

	if r.DataSize == 0 {
		// A delete request in Titan. No data upload in Spartan.
		return []byte{}, nil
	}

	uploadData := make([]byte, r.DataSize)
	buf := make([]byte, 1)
	for i := 0; i < int(r.DataSize); i++ {
		n, err := r.readwriter.Read(buf)
		if err == io.EOF && n <= 0 {
			switch r.Protocol {
			case ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C: // Titan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "40 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			case ProtocolType_Spartan: // Spartan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "4 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			}
		} else if err != nil && err != io.EOF {
			switch r.Protocol {
			case ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C: // Titan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "40 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			case ProtocolType_Spartan: // Spartan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "4 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			}
		}

		uploadData[i] = buf[0]
	}
	//_, err := r.readwriter.Read(titanData)
	//titanData, err := io.ReadAll(io.LimitReader(r.readwriter, r.DataSize))

	r._data = uploadData
	return uploadData, nil
}

// Takes checksum hash of IP and returns it in a hex string
func (r *Request) IPHash() string {
	hasher := sha256.New()
	hasher.Write([]byte(r.IP))
	return hex.EncodeToString(hasher.Sum(nil))
}

// MD5 Hash of User Cert - usually used for Gemini and Titan. Returns empty string when there's no user cert.
func (r *Request) UserCert_MD5Hash() string {
	if r.UserCert == nil {
		return ""
	}
	return fmt.Sprintf("%x", md5.Sum(r.UserCert.Raw))
}

func (r *Request) UserCertHash_Gemini() string {
	return r.UserCert_MD5Hash()
}

// SHA256 Hash of User Cert - usually used for Misfin. Returns empty string when there's no user cert.
func (r *Request) UserCert_SHA256Hash() string {
	h := sha256.New()
	h.Write(r.UserCert.Raw)
	return hex.EncodeToString(h.Sum(nil))
}

func (r *Request) UserCertHash_Misfin() string {
	return r.UserCert_SHA256Hash()
}

// Gets the requested hostname for protocols that use those in requests
// If not used in request, then gets the hostname of the server.
func (r *Request) Hostname() string {
	if r.Protocol == ProtocolType_Scroll || r.Protocol == ProtocolType_Gemini || r.Protocol == ProtocolType_Spartan || r.Protocol == ProtocolType_Titan || r.Protocol == ProtocolType_Misfin_A || r.Protocol == ProtocolType_Misfin_B || r.Protocol == ProtocolType_Misfin_C {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.Hostname()
	} else { // TODO: Get server's hostname
		//return r.Server.Hostname
		return r.Server.DefaultHostname
	}
}

// Gets the requested path, removing hostname and scheme information for protocols that use those in requests
// Path is unescaped if part of URL (for protocols that use URL requests). Unescaping is unnecessary for Nex and Gopher.
func (r *Request) Path() string { // TODO: Escaped vs. Unescaped
	if r.Protocol == ProtocolType_Nex {
		return r.requestString
	} else if r.Protocol == ProtocolType_Gopher {
		path, _, _ := strings.Cut(r.requestString, "\t")
		path, _, _ = strings.Cut(path, "?")
		return path
	} else if r.Protocol == ProtocolType_Scroll || r.Protocol == ProtocolType_Gemini || r.Protocol == ProtocolType_Spartan || r.Protocol == ProtocolType_Titan || r.Protocol == ProtocolType_Misfin_A || r.Protocol == ProtocolType_Misfin_B || r.Protocol == ProtocolType_Misfin_C {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.Path
	}

	return r.requestString
}

// Gets the requested path, removing hostname and scheme information for protocols that use those in requests
// Path is escaped if part of URL (for protocols that use URL requests). NOTE: Nex and Gopher requests are not escaped.
func (r *Request) RawPath() string {
	if r.Protocol == ProtocolType_Nex {
		return r.requestString
	} else if r.Protocol == ProtocolType_Gopher {
		path, _, _ := strings.Cut(r.requestString, "\t")
		path, _, _ = strings.Cut(path, "?")
		return path
	} else if r.Protocol == ProtocolType_Scroll || r.Protocol == ProtocolType_Gemini || r.Protocol == ProtocolType_Spartan || r.Protocol == ProtocolType_Titan || r.Protocol == ProtocolType_Misfin_A || r.Protocol == ProtocolType_Misfin_B || r.Protocol == ProtocolType_Misfin_C {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.EscapedPath()
	}

	return r.requestString
}

// Limits the number of bytes of uploaded data to read when you call request.Query() or request.RawQuery().
// The default is 1024 to (somewhat) match Gemini. This only applies to .RawQuery() and .Query() calls. It
// does not apply to .GetUploadData() calls.
func (r *Request) SetSpartanQueryLimit(bytesize int64) {
	r._queryLimit = bytesize
}

// Gets the escaped query for protocols that support queries in requests.
// If Spartan, it gets the uploaded data (if there is any) as text and escapes it.
// If data is longer than Spartan Query Limit, then this method will return an error.
func (r *Request) RawQuery() (string, error) {
	if r.Protocol == ProtocolType_Spartan {
		// If there's already stored data, then use that.
		// Note that the stored _data is not escaped.
		if len(r._data) > 0 {
			return url.QueryEscape(string(r._data)), nil
		}

		if r._queryLimit == 0 {
			r._queryLimit = 1024
		}
		if r.DataSize == 0 {
			return "", nil
		} else if r.DataSize > r._queryLimit {
			return "", fmt.Errorf("upload data too long, limited to %d bytes", r._queryLimit)
		} else {
			data, err := r._getUploadData() // Call this version because r.UploadSupported is not required when getting qureies of limited length in Spartan
			return url.QueryEscape(string(data)), err
		}
	} else {
		return r._rawQuery, nil
	}
}

// Gets the query for protocols that support queries in requests. The returned query is unescaped.
// If Spartan, it gets the *raw* uploaded data (if there is any) as text. To get it query escaped, use request.RawQuery()
// If data is longer than Spartan Query Limit, then this method will return an error.
func (r *Request) Query() (string, error) {
	if r.Protocol == ProtocolType_Spartan {
		// If there's already stored data, then use that.
		// Note that the stored _data is not escaped.
		if len(r._data) > 0 {
			return string(r._data), nil
		}

		if r._queryLimit == 0 {
			r._queryLimit = 1024
		}
		if r.DataSize == 0 {
			return "", nil
		} else if r.DataSize > r._queryLimit {
			return "", fmt.Errorf("upload data too long, limited to %d bytes", r._queryLimit)
		} else {
			data, err := r._getUploadData() // Call this version because r.UploadSupported is not required when getting queries of limited length in Spartan
			return string(data), err
		}
	} else {
		rawQuery, _ := r.RawQuery()
		q, err := url.QueryUnescape(rawQuery)
		if err != nil {
			return "", err
		}
		return q, nil
	}
}

// NOTE: Should almost never be used
func (r *Request) Fragment() string {
	if r.Protocol == ProtocolType_Scroll || r.Protocol == ProtocolType_Gemini || r.Protocol == ProtocolType_Spartan || r.Protocol == ProtocolType_Titan || r.Protocol == ProtocolType_Misfin_A || r.Protocol == ProtocolType_Misfin_B || r.Protocol == ProtocolType_Misfin_C {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.Fragment
	} else {
		parts := strings.SplitAfterN(r.requestString, "#", 2)
		if len(parts) <= 1 {
			return ""
		} else {
			f, err := url.QueryUnescape(parts[1])
			if err != nil {
				return ""
			} else {
				return f
			}
		}
	}
}

// NOTE: Should almost never be used
func (r *Request) RawFragment() string {
	if r.Protocol == ProtocolType_Scroll || r.Protocol == ProtocolType_Gemini || r.Protocol == ProtocolType_Spartan || r.Protocol == ProtocolType_Titan || r.Protocol == ProtocolType_Misfin_A || r.Protocol == ProtocolType_Misfin_B || r.Protocol == ProtocolType_Misfin_C {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.EscapedFragment()
	} else {
		parts := strings.SplitAfterN(r.requestString, "#", 2)
		if len(parts) <= 1 {
			return ""
		} else {
			return parts[1]
		}
	}
}

// Returns empty string if param not in route
func (r *Request) GetParam(name string) string {
	p, ok := r.params[name]
	if ok {
		return p
	}
	return ""
}

func (r *Request) Redirect(format string, elem ...any) { // TODO: Add query back on?
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Scroll:
		r.sendHeader("30 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		r.sendHeader("3 "+format+"\r\n", elem...)
	case ProtocolType_Nex:
		r.NexListing(fmt.Sprintf("=> "+format+" Redirect\n", elem...))
	case ProtocolType_Gopher: // TODO: Assumes gophermap link
		redirect := fmt.Sprintf(format, elem...)
		url, _ := url.Parse(redirect)
		if url.Scheme != "" {
			r.GophermapLine("h", "Redirect", "URL:"+url.String(), r.Hostname(), r.Server.ServePort)
		} else if strings.HasPrefix(redirect, "/") { // Absolute link
			link := path.Join(r.proxiedUnder, url.String())
			if strings.HasSuffix(redirect, "/") { // Add back in trailing slash, since path.Join removes it
				link += "/"
			}
			r.GophermapLine(string(r.gopherItemType), "Redirect", strings.TrimSpace(link), r.Hostname(), r.Server.ServePort)
		} else { // Relative link
			link := path.Join(r.proxiedUnder, r.Path(), url.String())
			if strings.HasSuffix(redirect, "/") { // Add back in trailing slash, since path.Join removes it
				link += "/"
			}
			r.GophermapLine(string(r.gopherItemType), "Redirect", strings.TrimSpace(link), r.Hostname(), r.Server.ServePort)
		}
	}

	URL, _ := url.Parse(fmt.Sprintf(format, elem...))
	if URL.Hostname() == r.Hostname() || !URL.IsAbs() { // Same hostname or no hostname provided
		expectedRedirectPath := URL.EscapedPath() // TODO: Should this be escaped or unescaped (especially for nex and gopher)?
		r.Server.IPRateLimit_ExpectRedirectPath(r.IP, expectedRedirectPath)
	}
}

func (r *Request) EnableConvertMode() {
	r.convertMode = true
}

func (r *Request) DisableConvertMode() {
	r.convertMode = false
}

// Sends a link to a particular route on the given server.
// Route links can reference other servers by prefixing the route path with "$ServerName".
// You can reference the current request's params by prefixing them with "$" in the route path.
/*
func (r *Request) RouteLink(route string, name string) error {
	preceedingSlash := strings.HasPrefix(route, "/")
	trailingSlash := strings.HasSuffix(route, "/")
	parts := strings.Split(route, "/")

	crossServer := false
	scheme := ""
	hostname := r.Hostname()
	port := r.Server.ServePort
	resultPath := ""

	// Find desired server and replace with host:port of server
	if strings.HasPrefix(parts[0], "$") {
		handle := r.Server.FindServerByName(strings.TrimPrefix(parts[0], "$"))
		server := handle.GetServer()

		scheme = server.Scheme()
		hostname = server.Hostname // TODO: Handle subdomains
		port = server.Port
		parts = parts[1:]
		resultPath = "/"
		crossServer = true
	} else if preceedingSlash {
		resultPath = "/"
	}

	// Find each param reference and replace with the param's value
	for _, component := range parts {
		if strings.HasPrefix(component, "$") {
			param := r.GetParam(strings.TrimPrefix(component, "$"))
			resultPath = path.Join(resultPath, param)
		} else {
			resultPath = path.Join(resultPath, component)
		}
	}

	if trailingSlash {
		resultPath = resultPath + "/"
	}

	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Nex, ProtocolType_Spartan, ProtocolType_Scroll:
		if crossServer {
			return r.Gemini(fmt.Sprintf("=> %s%s:%s%s %s\n", scheme, hostname, port, resultPath, name))
		} else {
			return r.Gemini(fmt.Sprintf("=> %s %s\n", resultPath, name))
		}
	case ProtocolType_Gopher:
		if crossServer {
			// TODO: Make sure this is correct
			return r.GophermapLine("h", name, resultPath, "URL:"+scheme+hostname, port)
		} else {
			// TODO: Detect what the route's linetype is!
			return r.GophermapLine("0", name, resultPath, hostname, port)
		}
	}

	return nil
}
*/

// Uses a regular link linetype for Gemini and Nex, and a prompt/search linetype for Spartan and Gopher.
// The link should be in standard url format, even for gopher servers.
func (r *Request) PromptLine(link string, text string) error {
	link_without_query_and_fragment, _, _ := strings.Cut(link, "#")
	link_without_query_and_fragment, _, _ = strings.Cut(link_without_query_and_fragment, "?")

	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Nex, ProtocolType_Scroll:
		return r.success([]byte(fmt.Sprintf("=> %s %s\n", link, text)))
	case ProtocolType_Spartan:
		return r.success([]byte(fmt.Sprintf("=: %s %s\n", link, text)))
	case ProtocolType_Gopher:
		URL, _ := url.Parse(link)
		if URL.Scheme != "" {
			return r.success([]byte(gophermapLine("7", text, "URL:"+URL.String(), r.Hostname(), r.Server.ServePort)))
		} else if strings.HasPrefix(link, "/") { // Absolute link
			newLink := path.Join(r.proxiedUnder, URL.EscapedPath())
			if strings.HasSuffix(link_without_query_and_fragment, "/") { // NOTE: Since path.Join removes trailing slashes, add them back
				newLink += "/"
			}
			if text == "" {
				text = newLink
			}
			if URL.RawQuery != "" {
				newLink += "?" + URL.RawQuery // Add the query back in
			}
			if URL.EscapedFragment() != "" {
				newLink += "#" + URL.EscapedFragment() // Add the fragment back in
			}
			r.success([]byte(gophermapLine("7", text, newLink, r.Hostname(), r.Server.ServePort)))
		} else { // Relative link
			newLink := path.Join(r.proxiedUnder, r.Path(), URL.EscapedPath())
			if strings.HasSuffix(link_without_query_and_fragment, "/") { // Add back in the trailing slash
				newLink += "/"
			}
			if text == "" {
				text = newLink
			}
			if URL.RawQuery != "" {
				newLink += "?" + URL.RawQuery // Add the query back in
			}
			if URL.EscapedFragment() != "" {
				newLink += "#" + URL.EscapedFragment() // Add the fragment back in
			}
			r.success([]byte(gophermapLine("7", text, newLink, r.Hostname(), r.Server.ServePort)))
		}
	}

	return nil
}

func (r *Request) Spartan(text string) error {
	// TODO
	return nil
}

// TODO: Add a way to configure nex width in Request (or Nex Server)
func (r *Request) Gemini(text string) error {
	if r.convertMode {
		switch r.Protocol {
		case ProtocolType_Gemini, ProtocolType_Titan:
			return r.success([]byte(text))
		case ProtocolType_Scroll:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiProxyLinks(strings.NewReader(text), r.Protocol == ProtocolType_Gopher, r.proxiedUnder, r.readwriter)
		case ProtocolType_Spartan:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToSpartan(strings.NewReader(text), r.proxiedUnder, r.readwriter)
			//return r.success([]byte(geminiToSpartan(text, r.proxiedUnder)))
		case ProtocolType_Nex:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToNex(strings.NewReader(text), 70, r.proxiedUnder, r.readwriter)
			//return r.success([]byte(geminiToNex(text, 70, r.proxiedUnder)))
		case ProtocolType_Gopher:
			/*currentPath := r.Path()
			if r.Proxied {
				// Add on the proxiedUnder path
				currentPath = r.proxiedUnder + r.Path()
			}*/
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToGophermap(strings.NewReader(text), 70, r.proxiedUnder, r.Path(), r.proxyRoute, r.Hostname(), r.Server.ServePort, r.readwriter)
			//return r.success([]byte(geminiToGophermap(text, 70, r.proxiedUnder, r.Path(), r.proxyRoute, r.Hostname(), r.Server.Port()))) // TODO
		}
	} else if r.Proxied {
		// Convert all links in the file
		err := r.successWithMimetype("text/gemini", []byte{})
		if err != nil {
			return err
		}
		geminiProxyLinks(strings.NewReader(text), r.Protocol == ProtocolType_Gopher, r.proxiedUnder, r.readwriter)
	} else {
		return r.successWithMimetype("text/gemini", []byte(text))
	}

	return nil
}

// TODO: Add a way to configure nex width in Request (or Nex Server)
func (r *Request) Scroll(text string) error {
	if r.convertMode {
		switch r.Protocol {
		case ProtocolType_Gemini, ProtocolType_Titan:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiProxyLinks(strings.NewReader(text), r.Protocol == ProtocolType_Gopher, r.proxiedUnder, r.readwriter)
		case ProtocolType_Scroll:
			return r.success([]byte(text))
		case ProtocolType_Spartan:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToSpartan(strings.NewReader(text), r.proxiedUnder, r.readwriter)
			//return r.success([]byte(geminiToSpartan(text, r.proxiedUnder)))
		case ProtocolType_Nex:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToNex(strings.NewReader(text), 70, r.proxiedUnder, r.readwriter)
			//return r.success([]byte(geminiToNex(text, 70, r.proxiedUnder)))
		case ProtocolType_Gopher:
			/*currentPath := r.Path()
			if r.Proxied {
				// Add on the proxiedUnder path
				currentPath = r.proxiedUnder + r.Path()
			}*/
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToGophermap(strings.NewReader(text), 70, r.proxiedUnder, r.Path(), r.proxyRoute, r.Hostname(), r.Server.ServePort, r.readwriter)
			//return r.success([]byte(geminiToGophermap(text, 70, r.proxiedUnder, r.Path(), r.proxyRoute, r.Hostname(), r.Server.Port()))) // TODO
		}
	} else if r.Proxied {
		// Convert all links in the file
		err := r.successWithMimetype("text/scroll", []byte{})
		if err != nil {
			return err
		}
		geminiProxyLinks(strings.NewReader(text), r.Protocol == ProtocolType_Gopher, r.proxiedUnder, r.readwriter)
	} else {
		return r.successWithMimetype("text/scroll", []byte(text))
	}

	return nil
}

func (r *Request) Markdown(text string) error {
	if r.convertMode {
		switch r.Protocol {
		case ProtocolType_Gemini, ProtocolType_Titan, ProtocolType_Spartan, ProtocolType_Scroll:
			return r.success([]byte(markdownToGemini(text)))
		case ProtocolType_Nex:
			return r.success([]byte(markdownToNex(text)))
		}
	} else if r.Proxied {
		// TODO
	}

	return r.successWithMimetype("text/markdown", []byte(text))
}

// Send Next text.
// TODO: This is unfinished.
func (r *Request) NexListing(text string) error {
	if r.convertMode {
		if r.Protocol == ProtocolType_Nex {
			return r.success([]byte(text))
		} else if r.Protocol == ProtocolType_Gemini || r.Protocol == ProtocolType_Spartan || r.Protocol == ProtocolType_Scroll || r.Protocol == ProtocolType_Titan { // Required to convert links to proxy links
			err := r.successWithMimetype("text/plain", []byte{})
			if err != nil {
				return err
			}
			nexProxyLinks(strings.NewReader(text), false, r.proxiedUnder, r.readwriter)
		} else if r.Protocol == ProtocolType_Gopher {
			currentPath := r.Path()
			if r.Proxied {
				// Add on the proxiedUnder path
				currentPath = r.proxiedUnder + r.Path()
			}
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			nexToGophermap(strings.NewReader(text), currentPath, r.Hostname(), r.Server.ServePort, 70, r.readwriter)
			//return r.success([]byte(nexToGophermap(text, currentPath, r.Hostname(), r.Server.Port(), 70)))
		}
	} else if r.Proxied {
		err := r.successWithMimetype("text/plain", []byte{})
		if err != nil {
			return err
		}
		nexProxyLinks(strings.NewReader(text), r.Protocol == ProtocolType_Gopher, r.proxiedUnder, r.readwriter)
	} else {
		return r.successWithMimetype("text/plain", []byte(text))
	}

	return nil
}

// TODO: make sure all lines end in CRLF
func (r *Request) Gophermap(text string) error {
	if r.convertMode {
		// TODO
	} else if r.Proxied {
		// gophermapProxyLinks()
	}

	return r.successWithMimetype("text/gophermap", []byte(text))
}

// Enter empty strings for hostname and port to use the server's hostname and port.
// TODO: Handle URL: links and cross-server links
func (r *Request) GophermapLine(linetype string, name string, selector string, hostname string, port string) error {
	isAbs := false
	if strings.HasPrefix(selector, "/") {
		isAbs = true
	}
	if hostname == "" {
		// TODO: Use request's hostname
		hostname = r.Hostname()
	}
	if port == "" {
		// Use request server's port
		port = r.Server.ServePort
	}
	if r.convertMode {
		switch r.Protocol {
		case ProtocolType_Gemini, ProtocolType_Nex, ProtocolType_Scroll, ProtocolType_Titan:
			if isAbs {
				return r.success([]byte(fmt.Sprintf("=> %s:%s%s %s\n", hostname, port, selector, name)))
			} else {
				// Assume relative selector is not going cross-server.
				return r.success([]byte(fmt.Sprintf("=> %s %s\n", selector, name)))
			}
		case ProtocolType_Spartan:
			if linetype == "7" {
				if isAbs {
					return r.PromptLine(fmt.Sprintf("%s:%s%s", hostname, port, selector), name)
				} else {
					return r.PromptLine(fmt.Sprintf("%s:%s%s", hostname, port, selector), name)
				}
			} else {
				if isAbs {
					return r.success([]byte(fmt.Sprintf("=> %s:%s%s %s\n", hostname, port, selector, name)))
				} else {
					// Assume relative selector is not going cross-server.
					return r.success([]byte(fmt.Sprintf("=> %s %s\n", selector, name)))
				}
			}
		}
	} else if r.Proxied {
		// TODO: gophermapProxyLinks()
	}
	result := fmt.Sprintf("%s%s\t%s\t%s\t%s\r\n", linetype, name, selector, hostname, port)
	return r.successWithMimetype("text/gophermap", []byte(result))
}

func (r *Request) PlainText(format string, elem ...any) error {
	if r.convertMode {
		// TODO
	}
	return r.successWithMimetype("text/plain", []byte(fmt.Sprintf(format, elem...)))
}

// TODO
func (r *Request) TextWithMimetype(mimetype string, text string) error {
	if r.convertMode {
		// TODO
	}
	return r.successWithMimetype(mimetype, []byte(text))
}
func (r *Request) Bytes(mimetype string, data []byte) error {
	if r.convertMode {
		// TODO
	}
	return r.successWithMimetype(mimetype, data)
}

// For sending an Abstract file. Will parse data to get Author metadata.
func (r *Request) Abstract(mimetype string, data string) error {
	abstractData := r.GetAbstractWithInlineMetadata(data)
	r.successWithMimetype(mimetype, nil) // Header and Metadata
	return r.PlainText("%s", abstractData)
}

func (r *Request) GetAbstractWithInlineMetadata(data string) (abstractData string) {
	abstractData = data

	// Scan first line of abstract to get metadata
	if !strings.HasPrefix(data, "#") {
		buf_reader := bufio.NewReader(strings.NewReader(data))
		for {
			line, err := buf_reader.ReadString('\n')
			if err != nil {
				break
			} else if strings.HasPrefix(line, "#") {
				rest, _ := io.ReadAll(buf_reader)
				abstractData = line + string(rest)
				break
			} else {
				field, value, hasValue := strings.Cut(line, ":")
				field = strings.ToLower(strings.TrimSpace(field))
				value = strings.TrimSpace(value)
				if hasValue {
					if field == "author" {
						r.scrollMetadata.Author = value
					} else if field == "publish-date" {
						publishdate, err := time.Parse(time.RFC3339, string(value))
						if err == nil {
							r.scrollMetadata.PublishDate = publishdate
						}
					} else if field == "modification-date" || field == "modified-date" || field == "update-date" || field == "updated-date" {
						modificationdate, err := time.Parse(time.RFC3339, string(value))
						if err == nil {
							r.scrollMetadata.UpdateDate = modificationdate
						}
					} else if field == "language" {
						r.SetLanguage(value)
					} else if field == "udc-class" {
						// Check if integer first, if not, then parse as string
						classification, err := strconv.Atoi(value)
						r.scrollMetadata.Classification = ScrollResponseUDC(classification)
						if err != nil {
							r.scrollMetadata.Classification = ClassificationStringToUDC(value)
						}
					}
				}
			}
		}
	}

	return abstractData
}

// Reads whole file into memory, automatically determins the correct mimetype, and sends it over (converting if in convert mode).
// If file not found or error on opening file, returns with error without sending anything back to the connection.
// Prefer FileMimetype() if you know the mimetype beforehand.
func (r *Request) File(filePath string) error {
	// Open file, get file mimetype, convert if convertMode is set, and send file
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	stat_times, _ := times.Stat(filePath)
	if stat_times.HasBirthTime() {
		r.scrollMetadata.PublishDate = stat_times.BirthTime().UTC()
	}
	r.scrollMetadata.UpdateDate = stat_times.ModTime().UTC()

	// NOTE: We can skip the mimetype detection for protocols that don't require it if we are not in convert mode
	mt := ""
	extension := filepath.Ext(filePath)
	if r.convertMode || r.Proxied || r.Protocol == ProtocolType_Gemini || r.Protocol == ProtocolType_Spartan || r.Protocol == ProtocolType_Scroll || r.Protocol == ProtocolType_Titan {
		mt = "text/plain"
		if strings.HasSuffix(filePath, ".gmi") || strings.HasSuffix(filePath, ".gemini") {
			mt = "text/gemini"
		} else if strings.HasSuffix(filePath, ".scroll") {
			mt = "text/scroll"
		} else if strings.HasSuffix(filePath, "index") || strings.HasSuffix(filePath, ".nex") {
			mt = "text/nex" // TODO: Assume nex for this for now, but come up with a better way later.
		} else if strings.HasSuffix(filePath, "gophermap") {
			mt = "text/gophermap"
		} else if extension == ".pem" {
			mt = "application/x-pem-file"
		} else {
			mime, err := mimetype.DetectReader(file)
			if err != nil {
				return err
			}
			mt = mime.String()
		}

		_, err = file.Seek(0, io.SeekStart)
		if err != nil {
			return err
		}
	}

	// Security warnings
	if CertFilename(filePath) {
		if !r.allowServeCert {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] An SSL cert/key file ('%s') is being served.\n", filePath)
	} else if PrivacySensitiveFilename(filePath) {
		return r.TemporaryFailure("File not allowed.")
	}
	if DotFilename(filePath) {
		if !r.allowServeDotFiles {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] Serving a dot file ('%s')\n", filePath)
	}

	// Get Audio File tag metadata
	switch mt {
	case "audio/mpeg", "audio/mp3", "audio/x-mpeg", "audio/ogg", "audio/x-ogg", "application/ogg", "application/x-ogg", "audio/flac", "audio/x-flac", "audio/wav", "audio/wave", "audio/x-wav", "audio/vnd.wave", "audio/mp4", "video/mp4":
		tagInfo, err := tag.ReadFrom(file)
		if err == nil {
			r.scrollMetadata.Author = tagInfo.AlbumArtist()
			if r.scrollMetadata.Author == "" {
				r.scrollMetadata.Author = tagInfo.Artist()
			}

			track, totalTracks := tagInfo.Track()
			r.scrollMetadata.PublishDate = time.Date(tagInfo.Year(), 0, 0, 0, 0, 0, 0, time.UTC)
			disc, totalDiscs := tagInfo.Disc()
			composer := tagInfo.Composer()
			r.scrollMetadata.Abstract = fmt.Sprintf("# %s\n, Track: %d/%d\nArtist: %s\nAlbum: %s\nAlbum Artist: %s\nComposer: %s\nDisc: %d/%d\n", tagInfo.Title(), track, totalTracks, tagInfo.Artist(), tagInfo.Album(), tagInfo.AlbumArtist(), composer, disc, totalDiscs)
		}
		file.Seek(0, io.SeekStart)
	}

	// If only Metadata requested, via Scroll Protocol, then don't serve the actual file but just its metadata
	if r.ScrollMetadataRequested {
		if r.scrollMetadata.Abstract != "" {
			return r.Abstract(mt, r.scrollMetadata.Abstract)
		} else {
			// Get abstract file
			abstract_file, err := os.Open(filePath + ".abstract")
			if err != nil {
				// Abstract file not found
				title := ""

				// If a gemtext/scrolltext/markdown file, get the title from the file
				if mt == "text/gemini" || mt == "text/markdown" || mt == "text/scroll" {
					title_, author, publishdate, updatedate, language, classification := GetInlineMetadataFromGemtext(file)
					title = title_
					if author != "" {
						r.scrollMetadata.Author = author
					}
					if (publishdate != time.Time{}) {
						r.scrollMetadata.PublishDate = publishdate
					}
					if (updatedate != time.Time{}) {
						r.scrollMetadata.UpdateDate = updatedate
					}
					if language != "" {
						r.scrollMetadata.Language = language
					}
					if classification != ScrollResponseUDC_Unclassed {
						r.scrollMetadata.Classification = classification
					}
				}

				// Set title to filename if it is empty
				if title == "" {
					title = "# " + filepath.Base(file.Name())
				}
				// Send blank abstract with title
				r.PlainText(title + "\n")
				return nil
			}
			defer abstract_file.Close()

			abstract_data, _ := io.ReadAll(abstract_file)
			return r.Abstract(mt, string(abstract_data))
		}
	} else {
		// Get Abstract info
		abstract_file, err := os.Open(filePath + ".abstract")
		if err == nil {
			defer abstract_file.Close()
			abstract_data, _ := io.ReadAll(abstract_file)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(string(abstract_data))
		} else if mt == "text/gemini" || mt == "text/scroll" || mt == "text/markdown" {
			// Get the info from the gemtext/scrolltext/markdown file itself
			headerData := ScanUpToTitle(file)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(headerData)
		}
	}

	// Check if Convert Mode or if proxied. If proxied, the called functions will change the links in the document to the proxy links.
	// If in convert mode, the document will fully convert to the default documenttype of the server. This is usually used for directory listings/menus.
	if r.convertMode || r.Proxied {
		// TODO: Change this to convert as it streams
		data, err := io.ReadAll(file)
		if err != nil {
			return err
		}
		if mt == "text/gemini" {
			err = r.Gemini(string(data))
		} else if mt == "text/scroll" {
			err = r.Scroll(string(data))
		} else if mt == "text/nex" {
			err = r.NexListing(string(data))
		} else if mt == "text/markdown" {
			err = r.Markdown(string(data))
		} else if mt == "text/gophermap" {
			err = r.Gophermap(string(data))
		} else {
			r.successWithMimetype(mt, nil)
			file.Seek(0, io.SeekStart)
			_, err = io.Copy(r.readwriter, file)
		}
		return err
	}

	// TODO: We still need to convert links within the document if the file is a nex listing or gemtext file.

	r.successWithMimetype(mt, nil)
	_, err = io.Copy(r.readwriter, file)
	return err
}

func (r *Request) FileMimetype(mimetype string, filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	stat_times, _ := times.Stat(filePath)
	if stat_times.HasBirthTime() {
		r.scrollMetadata.PublishDate = stat_times.BirthTime().UTC()
	}
	r.scrollMetadata.UpdateDate = stat_times.ModTime().UTC()

	// Security warnings
	if CertFilename(filePath) || CertMimetype(mimetype) {
		if !r.allowServeCert {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] An SSL cert/key file ('%s') is being served.\n", filePath)
	} else if PrivacySensitiveFilename(filePath) || PrivacySensitiveMimetype(mimetype) {
		return r.TemporaryFailure("File not allowed.")
	}
	if DotFilename(filePath) {
		if !r.allowServeDotFiles {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] Serving a dot file ('%s')\n", filePath)
	}

	// Get Audio File tag metadata
	switch mimetype {
	case "audio/mpeg", "audio/mp3", "audio/x-mpeg", "audio/ogg", "audio/x-ogg", "application/ogg", "application/x-ogg", "audio/flac", "audio/x-flac", "audio/wav", "audio/wave", "audio/x-wav", "audio/vnd.wave", "audio/mp4", "video/mp4":
		tagInfo, err := tag.ReadFrom(file)
		if err == nil {
			r.scrollMetadata.Author = tagInfo.AlbumArtist()
			if r.scrollMetadata.Author == "" {
				r.scrollMetadata.Author = tagInfo.Artist()
			}

			track, totalTracks := tagInfo.Track()
			r.scrollMetadata.PublishDate = time.Date(tagInfo.Year(), 0, 0, 0, 0, 0, 0, time.UTC)
			disc, totalDiscs := tagInfo.Disc()
			composer := tagInfo.Composer()
			r.scrollMetadata.Abstract = fmt.Sprintf("# %s\n, Track: %d/%d\nArtist: %s\nAlbum: %s\nAlbum Artist: %s\nComposer: %s\nDisc: %d/%d\n", tagInfo.Title(), track, totalTracks, tagInfo.Artist(), tagInfo.Album(), tagInfo.AlbumArtist(), composer, disc, totalDiscs)
		}
		file.Seek(0, io.SeekStart)
	}

	// If only Metadata requested, via Scroll Protocol, then don't serve the actual file but just its metadata
	if r.ScrollMetadataRequested {
		// Abstract
		if r.scrollMetadata.Abstract != "" {
			return r.Abstract(mimetype, r.scrollMetadata.Abstract)
		} else {
			// Get abstract file
			abstract_file, err := os.Open(filePath + ".abstract")
			if err != nil {
				// Abstract file not found
				title := ""

				// If a gemtext/scrolltext/markdown file, get the title from the file
				if mimetype == "text/gemini" || mimetype == "text/markdown" || mimetype == "text/scroll" {
					title_, author, publishdate, updatedate, language, classification := GetInlineMetadataFromGemtext(file)
					title = title_
					if author != "" {
						r.scrollMetadata.Author = author
					}
					if (publishdate != time.Time{}) {
						r.scrollMetadata.PublishDate = publishdate
					}
					if (updatedate != time.Time{}) {
						r.scrollMetadata.UpdateDate = updatedate
					}
					if language != "" {
						r.scrollMetadata.Language = language
					}
					if classification != ScrollResponseUDC_Unclassed {
						r.scrollMetadata.Classification = classification
					}
				}

				// Set title to filename if it is empty
				if title == "" {
					title = "# " + filepath.Base(file.Name())
				}
				// Send blank abstract with title
				r.PlainText(title + "\n")
				return nil
			}
			defer abstract_file.Close()

			abstract_data, _ := io.ReadAll(abstract_file)
			return r.Abstract(mimetype, string(abstract_data))
		}
	} else {
		// Get Abstract info
		abstract_file, err := os.Open(filePath + ".abstract")
		if err == nil {
			defer abstract_file.Close()
			abstract_data, _ := io.ReadAll(abstract_file)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(string(abstract_data))
		} else if mimetype == "text/gemini" || mimetype == "text/scroll" || mimetype == "text/markdown" {
			// Get the info from the gemtext/scrolltext/markdown file itself
			headerData := ScanUpToTitle(file)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(headerData)
		}
	}

	// Check if Convert Mode or if proxied. If proxied, the called functions will change the links in the document to the proxy links.
	// If in convert mode, the document will fully convert to the default documenttype of the server. This is usually used for directory listings/menus.
	if r.convertMode || r.Proxied {
		// TODO: Change this to convert as it streams
		data, err := io.ReadAll(file)
		if err != nil {
			return err
		}
		if mimetype == "text/gemini" {
			err = r.Gemini(string(data))
		} else if mimetype == "text/scroll" {
			err = r.Scroll(string(data))
		} else if mimetype == "text/nex" {
			err = r.NexListing(string(data))
		} else if mimetype == "text/markdown" {
			err = r.Markdown(string(data))
		} else if mimetype == "text/gophermap" {
			err = r.Gophermap(string(data))
		} else {
			r.successWithMimetype(mimetype, nil)
			file.Seek(0, io.SeekStart)
			_, err = io.Copy(r.readwriter, file)
		}
		return err
	}

	r.successWithMimetype(mimetype, nil)
	_, err = io.Copy(r.readwriter, file)
	return err
}

// TODO: Use a buffer bool and provide a 32KB buffer from the pool.
// Streams a file, using a 32KB buffer by default. If you want to control the buffer size, use StreamBuffer.
func (r *Request) Stream(mimetype string, reader io.Reader) error {
	r.successWithMimetype(mimetype, nil)
	_, err := io.Copy(r.readwriter, reader)
	return err
}

// Provide a buffer to handle the copy. This allows you to control your own buffer size to increase performance. For big files, create a big buffer (at the cost of memory). For small files, create a smaller buffer.
func (r *Request) StreamBuffer(mimetype string, reader io.Reader, buf []byte) error {
	r.successWithMimetype(mimetype, nil)
	_, err := io.CopyBuffer(r.readwriter, reader, buf)
	return err
}

func (r *Request) NotFound(format string, elem ...any) error {
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan:
		return r.sendHeader("51 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("4 "+format+"\r\n", elem...)
	case ProtocolType_Nex:
		result := "Error: " + fmt.Sprintf(format, elem...) + "\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	case ProtocolType_Gopher:
		// TODO: What should the selector be? And does it matter?
		result := "3Error: " + fmt.Sprintf(format, elem...) + "\t/\t" + r.Hostname() + "\t" + r.Server.ServePort + "\r\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	}

	return nil
}

func (r *Request) TemporaryFailure(format string, elem ...any) error {
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan:
		return r.sendHeader("40 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 "+format+"\r\n", elem...) // TODO: Distinguish between temporary server error and client error
	case ProtocolType_Nex:
		result := "Temporary Failure: " + fmt.Sprintf(format, elem...) + "\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	case ProtocolType_Gopher:
		result := "3Temporary Failure: " + fmt.Sprintf(format, elem...) + "\t/\t" + r.Hostname() + "\t" + r.Server.ServePort + "\r\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	}

	return nil
}

func (r *Request) BadRequest(format string, elem ...any) error {
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan:
		return r.sendHeader("59 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("4 "+format+"\r\n", elem...)
	case ProtocolType_Nex:
		result := "Bad Request: " + fmt.Sprintf(format, elem...) + "\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	case ProtocolType_Gopher:
		result := "3Bad Request: " + fmt.Sprintf(format, elem...) + "\t/\t" + r.Hostname() + "\t" + r.Server.ServePort + "\r\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	}

	return nil
}

// TODO: When proxying gopher to gemini, translate the gopher input requested message/error to a gemini input request.
func (r *Request) RequestInput(format string, elem ...any) error {
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan:
		return r.sendHeader("10 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		// Since spartan has no input request response, send over gemtext with a prompt linetype link that goes to the same url/path as this current page
		selector := r.Path()
		if r.Proxied {
			selector = path.Join(r.proxiedUnder, r.Path())
			if strings.HasSuffix(r.Path(), "/") { // Add trailing slash back in, since path.Join strips it
				selector += "/"
			}
		}
		return r.PromptLine(selector, fmt.Sprintf(format, elem...))
	case ProtocolType_Nex:
		// Doesn't support input
		return r.TemporaryFailure("Nex doesn't support input.")
	case ProtocolType_Gopher:
		selector := r.Path()
		if r.Proxied {
			selector = path.Join(r.proxiedUnder, r.Path())
			if strings.HasSuffix(r.Path(), "/") { // Add trailing slash back in, since path.Join strips it
				selector += "/"
			}
		}
		return r.GophermapLine("7", fmt.Sprintf(format, elem...), selector, r.Hostname(), r.Server.ServePort)
		// Does support input // TODO
	}
	return nil
}

func (r *Request) RequestClientCert(format string, elem ...any) error {
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan:
		return r.sendHeader("60 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 Spartan doesn't support client certs.\r\n") // Considered a server error
	case ProtocolType_Nex:
		return r.TemporaryFailure("Nex doesn't support client certs.")
	case ProtocolType_Gopher:
		// Doesn't support client certs // TODO
	}
	return nil
}

func (r *Request) ClientCertNotAuthorized(format string, elem ...any) error {
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan:
		return r.sendHeader("61 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 Spartan doesn't support client certs.\r\n") // Considered a server error
	case ProtocolType_Nex:
		return r.TemporaryFailure("Nex doesn't support client certs.\n")
	case ProtocolType_Gopher:
		// Doesn't support client certs // TODO
	}
	return nil
}

func (r *Request) ServerUnavailable(format string, elem ...any) error {
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan:
		return r.sendHeader("41 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 "+format+"\r\n", elem...)
	case ProtocolType_Nex:
		r.TemporaryFailure("Server Unavailable: "+format+"\n", elem)
	case ProtocolType_Gopher:
		// TODO: Determine if current route is a gophermap or text (or some other itemtype)
	}
	return nil
}

func (r *Request) success(data []byte) error {
	defaultMimetype := "text/plain"
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Spartan, ProtocolType_Titan:
		defaultMimetype = "text/gemini"
	case ProtocolType_Nex:
		defaultMimetype = "text/nex"
	case ProtocolType_Gopher:
		defaultMimetype = "text/gophermap"
	case ProtocolType_Scroll:
		defaultMimetype = "text/scroll"
	}

	return r.successWithMimetype(defaultMimetype, data)
}

func (r *Request) successWithMimetype(mimetype string, data []byte) error {
	if r.scrollMetadata.Language != "" && r.scrollMetadata.Language != "none" { // Should use BCP47
		parsedlang, err := language.Parse(r.scrollMetadata.Language)
		if err == nil {
			mimetype = mimetype + "; lang=" + parsedlang.String()
		}
	} else if r.scrollMetadata.Language != "none" {
		// If mimetype is a textual natural-language type, then add the lang parameter if it doesn't already exist
		mediatype, params, err := mime.ParseMediaType(mimetype)
		if _, ok := params["lang"]; !ok && err == nil && MimetypeGetsLangParam(mediatype) {
			defaultLanguage := r.Host.Server.DefaultLanguage
			parsedLang, err := language.Parse(defaultLanguage)
			if err == nil && defaultLanguage != "" {
				mimetype = mimetype + "; lang=" + parsedLang.String()
			}
		}
	}

	var err error
	switch r.Protocol {
	case ProtocolType_Gemini, ProtocolType_Titan:
		err = r.sendHeader("20 %s\r\n", mimetype)
		if err != nil {
			return err
		}
	case ProtocolType_Scroll:
		statusCode := UDCToSuccessResponse(r.scrollMetadata.Classification)
		err = r.sendHeader("%d %s\r\n%s\r\n%s\r\n%s\r\n", statusCode, mimetype, r.scrollMetadata.Author, r.scrollMetadata.PublishDate.UTC().Format(time.RFC3339), r.scrollMetadata.UpdateDate.UTC().Format(time.RFC3339))
		if err != nil {
			return err
		}
	case ProtocolType_Spartan:
		err = r.sendHeader("2 %s\r\n", mimetype)
		if err != nil {
			return err
		}
	}

	if len(data) > 0 {
		_, err = r.readwriter.Write(data)
	}
	return err
}

func (r *Request) sendHeader(format string, elem ...any) error {
	if r.headerSent {
		return nil
	}
	header := fmt.Sprintf(format, elem...)
	if header != "" {
		_, err := r.readwriter.Write([]byte(header))
		r.headerSent = true
		return err
	}
	return nil
}

// Proxy Handler - handles proxying routes from one server to another, or to a route on the same server
/*
func proxyHandler(request Request) {
	//fmt.Printf("Glob: %s\n", request.GlobString)

	// TODO: As a security mechanism, make sure we do not get infinite proxying when having two servers that proxy each other.
	// Ideally, if we find that this request is proxying a route that proxies back to the originating server, just redirect to that page.
	// Technically, the request header limit also limites the depth of the proxying, but we should still have this here just in case
	// (and for protocols in the future that might have a much larger header length limit).

	proxyRoute := request.proxyRoute
	var server *Server = nil
	if strings.HasPrefix(proxyRoute, "$") {
		trimmed := strings.TrimPrefix(proxyRoute, "$")
		if strings.HasPrefix(trimmed, "/") {
			// If starts with $ but there's no server name ("$/"), assume the current server
			proxyRoute = trimmed
		} else {
			parts := strings.SplitN(trimmed, "/", 2)
			destServerName := parts[0]
			proxyRoute = parts[1]
			handle := request.Server.SIS().FindServerByName(destServerName)
			server = handle.GetServer()
		}
	}
	if server == nil {
		server = request.Server.GetServer()
	}
	proxyRoute = strings.TrimPrefix(proxyRoute, "/")

	resultRouteString := InsertParamsIntoRouteString(proxyRoute, request.params, request.GlobString, false)

	// TODO: Check escaping of params
	node, globString, params := server.Router.Search(resultRouteString)
	if node != nil && node.Handler != nil {
		proxyRequest := request
		switch server.Type {
		case ServerType_Nex, ServerType_Admin, ServerType_Gemini, ServerType_Gopher, ServerType_Spartan:
			proxyRequest.requestString = resultRouteString
			// TODO
		}

		// NOTE: When proxying to a Spartan server, since spartan doesn't have proper query strings because
		// they act like normal data uploads, when the query or upload data is requested in the spartan
		// server, it will first check if _data is non-empty, and if so, it will use that as the data upload.
		// NOTE: However, when proxying *from* a Spartan server, do not get the query, since that will start
		// reading the data upload. Instead, we want to delay the data upload to when the handler requests it.
		if request.Type != ServerType_Spartan { // Get query when not proxying *from* a spartan server.
			query, _ := request.Query()
			proxyRequest._data = []byte(query)
			proxyRequest.DataMime = "text/plain"
		}

		//proxyRequest.requestString = // TODO: How do I do this?
		proxyRequest.GlobString = globString
		proxyRequest.params = params
		// proxyRequest.Data = ???
		proxyRequest.servePath = node.servePath
		proxyRequest.convertMode = true
		proxyRequest.proxyRoute = node.proxyRoute
		proxyRequest.fromRoute = node.GetRoutePath()
		proxyRequest.proxiedFromRoute = request.fromRoute
		fmt.Printf("Proxied from route '%s' to route '%s'\n", proxyRequest.proxiedFromRoute, proxyRequest.fromRoute)

		// If gopherItemType of node was assigned, then use that. Otherwise, continue to use the ProxyRoute's assigned gopher type
		if node.gopherItemType != 0 {
			proxyRequest.gopherItemType = node.gopherItemType
		}

		// TODO
		if request.Proxied {
			proxyRequest.proxiedUnder = path.Join(request.proxiedUnder, strings.TrimSuffix(request.Path(), request.GlobString))
			//fmt.Printf("Proxying Under: %s\nOrig Request: %s\n", proxyRequest.proxiedUnder, proxyRequest.origRequest)
		} else {
			proxyRequest.proxiedUnder = "/" + strings.TrimSuffix(request.Path(), request.GlobString)
			//proxyRequest.origRequest = "/" + path.Join(request.origRequest, request.Path())
			//fmt.Printf("Proxying Under: %s\nOrig Request: %s\n", proxyRequest.proxiedUnder, proxyRequest.origRequest)
		}
		proxyRequest.Proxied = true
		// proxyRequest.Server // TODO: For now, the proxyRequest.Server is the source Server, not the one being proxied. Look more into this, or perhaps add another field as proxyRequest.ProxyServer
		node.Handler(proxyRequest)
	} else {
		request.NotFound("Proxy route not found.")
	}
}
*/

// Reverse will insert params into components starting with ":". Otherwise, params will be inserted into components starting with "$"
func InsertParamsIntoRouteString(route string, params map[string]string, globString string, reverse bool) string {
	resultRouteString := "/"
	parts := strings.Split(route, "/")
	for _, component := range parts {
		if !reverse && strings.HasPrefix(component, "$") {
			param := params[strings.TrimPrefix(component, "$")]
			resultRouteString = path.Join(resultRouteString, param)
		} else if reverse && strings.HasPrefix(component, ":") {
			param := params[strings.TrimPrefix(component, ":")]
			resultRouteString = path.Join(resultRouteString, param)
		} else if component == "*" {
			if !strings.HasSuffix(resultRouteString, "/") {
				resultRouteString = resultRouteString + "/" + globString
			} else {
				resultRouteString = resultRouteString + globString
			}
		} else {
			resultRouteString = path.Join(resultRouteString, component)
		}
	}

	return resultRouteString
}
