package main

import (
	"crypto"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/fs"
	"net"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"

	"github.com/djherbis/times"
	"github.com/gabriel-vasile/mimetype"
)

type Certificate struct {
	Certificate    *x509.Certificate
	PrivateKey     crypto.PrivateKey
	PrivateKeyData []byte
}

type Host struct {
	Hostname        string
	Directory       string
	CertificateFile string
	Default         bool
	Certificate     Certificate
	DownloadRouter  *Router
	//AbstractRouter  *Router
	UploadRouter *Router
	Server       *Server
}

func NewHost(server *Server) *Host {
	host := &Host{DownloadRouter: &Router{} /*AbstractRouter: &Router{},*/, UploadRouter: &Router{}, Server: server}
	/*host.Directory = filepath.Join(server.ServerDirectory, host.Hostname)
	os.Mkdir(host.Directory, 0600)*/

	return host
}

// Setup prepares Host for serving
func (host *Host) Setup() {
	if host.Directory == "" {
		host.Directory = filepath.Join(host.Server.ServerDirectory, host.Hostname)
		os.Mkdir(host.Directory, 0700)
	}
	if host.Directory == "/" || host.Directory == "C:" || host.Directory == "C:/" || host.Directory == "C:\\" {
		panic("System's root directory is exposed.")
	}
	host.DownloadRouter.addRoute("/*", scroll_handleDirectory, host.Directory, "", '1')
}

func (host *Host) loadCertificate() error {
	data, err := os.ReadFile(host.CertificateFile)
	if err != nil {
		return err
	}

	cert, privatekey, privatekey_data := getCertsFromPemData(data)
	host.Certificate = Certificate{cert, privatekey, privatekey_data}

	return nil
}

func (host *Host) HandleRequest(tlsConn *tls.Conn, requestHeader RequestHeader) {
	if requestHeader.Protocol != ProtocolType_Titan && requestHeader.Protocol != ProtocolType_Scroll {
		// Protocol not supported
		switch requestHeader.Protocol {
		case ProtocolType_Gemini, ProtocolType_Scroll:
			tlsConn.Write([]byte("53 Protocol not served.\r\n"))
		case ProtocolType_Titan:
			tlsConn.Write([]byte("53 Protocol not served.\r\n"))
		case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
			tlsConn.Write([]byte("53 Protocol not served.\r\n"))
		case ProtocolType_Gopher_SSL:
			tlsConn.Write([]byte(fmt.Sprintf("3%s.\t/\t%s\t%s\r\n", "Protocol not served.", host.Server.DefaultHostname, host.Server.ServePort)))
		}
		return
	}

	// Get the user's IP
	ip, _, _ := net.SplitHostPort(tlsConn.RemoteAddr().String())

	// Get the URL
	request_without_query, _, _ := strings.Cut(requestHeader.Request, "?")
	URL, err := url.Parse(requestHeader.Request)
	if err != nil {
		tlsConn.Write([]byte("59 Invalid url in request.\r\n"))
		return
	}

	fmt.Printf("[%s] Requested Path: %s\n", host.Hostname, URL.EscapedPath())
	/*if s.Type != ServerType_Admin {
		s.SIS.Log(s.Type, s.Name, "Requested Path: %s", URL.EscapedPath())
	}*/

	// Do rate-limiting now that we have read the header of the request to check against
	if host.Server.IpRateLimit_GetExpectedRedirectPath(ip) == URL.EscapedPath() {
		// Allow the redirect through and clear the expected redirect path
		host.Server.IPRateLimit_ClearRedirectPath(ip)
	} else if host.Server.IsIPRateLimited(ip) {
		// Rate limited, return a 44 slow down error
		tlsConn.Write([]byte("44 1\r\n"))
		return
	}

	if requestHeader.Protocol == ProtocolType_Titan {
		node, globString, params := host.UploadRouter.Search(URL.EscapedPath())

		// Call handlers. The data should be read from within these handlers
		if node != nil && node.Handler != nil {
			// Set token as param
			params["token"] = requestHeader.Token
			node.visits.Add(1)
			node.currentConnections.Add(1)
			node.Handler(Request{ProtocolType_Titan, true, node.GetRoutePath(), request_without_query, URL.RawQuery, 0, globString, tlsConn, params, requestHeader.ContentLength, []byte{}, requestHeader.Mime, requestHeader.UserCert, ip, node.servePath, false, false, node.proxyRoute, "", "", false, false, node.gopherItemType, false, host, host.Server, false, []string{}, ScrollMetadata{}})
			node.currentConnections.Add(-1)
		} else {
			host.UploadRouter.NotFoundHandler(Request{ProtocolType_Titan, true, "", request_without_query, URL.RawQuery, 0, globString, tlsConn, params, requestHeader.ContentLength, []byte{}, requestHeader.Mime, requestHeader.UserCert, ip, "", false, false, node.proxyRoute, "", "", false, false, node.gopherItemType, false, host, host.Server, false, []string{}, ScrollMetadata{}})
		}
		params = nil
	} else if requestHeader.Protocol == ProtocolType_Scroll {
		node, globString, params := host.DownloadRouter.Search(URL.EscapedPath())
		if node != nil && node.Handler != nil {
			node.visits.Add(1)
			node.currentConnections.Add(1)
			node.Handler(Request{ProtocolType_Scroll, false, node.GetRoutePath(), request_without_query, URL.RawQuery, 0, globString, tlsConn, params, 0, []byte{}, "", requestHeader.UserCert, ip, node.servePath, false, false, node.proxyRoute, "", "", false, false, node.gopherItemType, false, host, host.Server, requestHeader.MetadataRequest, requestHeader.Languages, ScrollMetadata{}})
			node.currentConnections.Add(-1)
		} else {
			host.DownloadRouter.NotFoundHandler(Request{ProtocolType_Scroll, false, "", request_without_query, URL.RawQuery, 0, globString, tlsConn, params, 0, []byte{}, "", requestHeader.UserCert, ip, "", false, false, "", "", "", false, false, '1', false, host, host.Server, requestHeader.MetadataRequest, requestHeader.Languages, ScrollMetadata{}})
		}
		params = nil
	}
}

func scroll_notFound(request Request) {
	request.NotFound("Not Found.")
}

func scroll_directoryNotFile(request Request) {
	request.TemporaryFailure("/%s/ is a directory, not a file.", request.requestString)
}

// TODO: Cache file's mimetype!!! // store in request?
func scroll_handleFile(request Request) {
	// Disable convert mode for files (directory index files will be handled by the handleDirectory method)
	if !(filepath.Ext(request.servePath) == ".scroll" && (request.Protocol == ProtocolType_Gemini || request.Protocol == ProtocolType_Spartan || request.Protocol == ProtocolType_Titan)) {
		request.DisableConvertMode()
	}

	err := request.File(request.servePath)
	if err != nil {
		scroll_notFound(request)
		return
	}
}

// TODO: NotFoundHandler vs. request.NotFound?????
func scroll_handleDirectory(request Request) {
	// Combine servePath with request path
	p := filepath.Join(request.servePath, request.GlobString)
	directoryName := filepath.Base(p)
	//fmt.Printf("Serving: %s\n", p)

	// Check if file, and serve the file instead
	info, err := os.Stat(p)
	if err != nil {
		scroll_notFound(request)
		return // Return error
	} else if info.IsDir() && !strings.HasSuffix(request.requestString, "/") { // TODO: request.TrailingSlash
		// If directory but request string doesn't end in "/", then redirect to proper route
		//gemini_directoryNotFile(request)
		request.Redirect(request.requestString + "/")
		return
	} else if !info.IsDir() {
		// Disable convert mode for files within directories (directory listing/index files will still be converted further below)
		if !(filepath.Ext(p) == ".scroll" && (request.Protocol == ProtocolType_Gemini || request.Protocol == ProtocolType_Spartan || request.Protocol == ProtocolType_Titan)) {
			request.DisableConvertMode()
		}

		// If not a directory, serve the file
		err := request.File(p)
		if err != nil {
			scroll_notFound(request)
			return
		}
		return
	}

	// Get the Abstract file for the directory (.abstract), if available
	if request.ScrollMetadataRequested {
		abstractFilepath := filepath.Join(p, ".abstract")
		abstractData, err := os.ReadFile(abstractFilepath)
		if err == nil {
			request.scrollMetadata.Abstract = string(abstractData)
		}
	}

	// Try index.scroll. If doesn't exist, then try index.gmi, and if that doesn't exist, then serve directory listing.
	indexFilepath := filepath.Join(p, "index.scroll")
	err = request.FileMimetype("text/scroll", indexFilepath)
	//indexFile, err := os.Open(indexFilepath)
	if err == nil {
		return
	} else if errors.Is(err, os.ErrNotExist) || errors.Is(err, fs.ErrNotExist) {
		indexFilepath := filepath.Join(p, "index.gmi")
		err = request.FileMimetype("text/gemini", indexFilepath)
		//indexFile, err := os.Open(indexFilepath)
		if err == nil {
			return
		}
	}

	// Set directory publishdate and modificationdate stuff
	stat_times, _ := times.Stat(p)
	if stat_times.HasBirthTime() {
		request.scrollMetadata.PublishDate = stat_times.BirthTime().UTC()
	}
	request.scrollMetadata.UpdateDate = stat_times.ModTime().UTC()

	if request.ScrollMetadataRequested {
		// If we get here, then the scroll/gemini files were not found. Send the abstract
		if request.scrollMetadata.Abstract != "" {
			request.Abstract("text/scroll", request.scrollMetadata.Abstract)
		} else {
			request.Abstract("text/scroll", "# "+directoryName+"\n")
		}
		return
	}

	// Check for .modified and .desc files, used for sorting the directory listing.
	modified := true
	if _, err := os.Stat(filepath.Join(p, ".modified")); err != nil {
		modified = false
	}
	asc := false
	if _, err := os.Stat(filepath.Join(p, ".desc")); err != nil {
		asc = true
	}

	entries, err := os.ReadDir(p)
	if err != nil {
		scroll_notFound(request)
		return
	}
	sort.Slice(entries, func(i, j int) bool {
		if modified {
			st1, err := entries[i].Info()
			if err != nil {
				return false // TODO
			}
			st2, err := entries[j].Info()
			if err != nil {
				return false
			}
			return st1.ModTime().After(st2.ModTime())
		} else if asc {
			return entries[i].Name() < entries[j].Name()
		} else {
			return entries[i].Name() > entries[j].Name()
		}
	})

	// Send the directory listing
	request.Scroll(fmt.Sprintf("# %s\n", directoryName))
	if request.requestString != "." { // TODO: Print ".." if the parent route has a handler?
		request.Scroll("=> ../ ../\n")
	}
	for _, entry := range entries {
		name := entry.Name()
		if filepath.Ext(name) == ".abstract" { // Hide abstract files
			continue
		}
		if CertFilename(name) && !request.allowServeCert {
			continue
		} else if PrivacySensitiveFilename(name) {
			continue
		}
		if DotFilename(name) && !request.allowServeDotFiles {
			continue
		}
		// TODO: Check permissions of file?
		info, err := entry.Info()
		if err != nil {
			continue
		}
		if info.Mode()&(1<<2) == 0 {
			continue
		}
		if entry.IsDir() {
			name = name + "/"
		}
		if request.Protocol == ProtocolType_Gopher && request.convertMode && !entry.IsDir() { // For proxying
			mt := ""
			extension := filepath.Ext(name)
			mt = "text/plain"
			if strings.HasSuffix(name, ".gmi") || strings.HasSuffix(name, ".gemini") {
				mt = "text/gemini"
			} else if strings.HasSuffix(name, ".scroll") {
				mt = "text/scroll"
			} else if strings.HasSuffix(name, "index") || strings.HasSuffix(name, ".nex") {
				mt = "text/nex" // TODO: Assume nex for this for now, but come up with a better way later.
			} else if strings.HasSuffix(name, "gophermap") {
				mt = "text/gophermap"
			} else if extension == ".pem" {
				mt = "application/x-pem-file"
			} else {
				path := filepath.Join(p, name)
				file, err := os.Open(path)
				if err != nil {
					continue
				}
				defer file.Close()

				mime, err := mimetype.DetectReader(file)
				if err != nil {
					continue
				}
				mt = mime.String()
			}

			itemtype := MimetypeToGopherItemtype(mt, "")
			request.GophermapLine(string(itemtype), name, path.Join(request.proxiedUnder, request.Path(), name), "", "")
		} else {
			request.Scroll(fmt.Sprintf("=> %s\n", name))
		}
	}
}
