package main

import (
	"bytes"
	"context"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"time"

	cmap "github.com/orcaman/concurrent-map/v2"
	"golang.org/x/sync/semaphore"
)

type ProtocolType int

const ProtocolType_Unknown ProtocolType = 0
const ProtocolType_Gemini ProtocolType = 1
const ProtocolType_Nex ProtocolType = 2
const ProtocolType_NPS ProtocolType = 3
const ProtocolType_Gopher ProtocolType = 4
const ProtocolType_Gopher_SSL ProtocolType = 5 // TODO
const ProtocolType_Misfin_A ProtocolType = 6   // Deprecated
const ProtocolType_Misfin_B ProtocolType = 7
const ProtocolType_Misfin_C ProtocolType = 8
const ProtocolType_Titan ProtocolType = 9
const ProtocolType_Spartan ProtocolType = 10
const ProtocolType_Scroll ProtocolType = 11

type RateLimitListItem struct {
	prevTime     time.Time
	redirectPath string
}

type Server struct {
	ServerDirectory          string
	BindAddress              string
	Port                     string // Port being bound to
	ServePort                string // Port to use in absolute links
	RateLimit                int    // In ms
	Hosts                    map[string]*Host
	DefaultHostname          string
	MaxConcurrentConnections int // Defaults to 2000
	ipBlocklist              cmap.ConcurrentMap[string, struct{}]
	ipRateLimitList          cmap.ConcurrentMap[string, RateLimitListItem]
	msg                      chan string
	maintenanceMode          bool
	DefaultLanguage          string
}

func NewServer(directory string) *Server {
	server := &Server{ServerDirectory: directory}
	server.Hosts = make(map[string]*Host)
	server.ipBlocklist = cmap.New[struct{}]()
	server.ipRateLimitList = cmap.New[RateLimitListItem]()
	server.msg = make(chan string)
	server.MaxConcurrentConnections = 2000
	return server
}

func (server *Server) BlockIP(ip string) {
	server.ipBlocklist.Set(ip, struct{}{})
}
func (server *Server) AllowIP(ip string) {
	server.ipBlocklist.Remove(ip)
}
func (server *Server) IsIPBlocked(ip string) bool {
	return server.ipBlocklist.Has(ip)
}

// If not rate-limited, set ip to current time and return false. If last access time is within duration, set ip to current time and return true. Otherwise, set ip to current time and return false
func (s *Server) IsIPRateLimited(ip string) bool {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))
	currentTime := time.Now().UTC()

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		if currentTime.After(rl.prevTime.Add(time.Duration(s.RateLimit) * time.Millisecond)) {
			// Outside of duration, no rate limit
			s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, rl.redirectPath})
			return false
		} else {
			// Rate limited, require ip to wait 1 whole second before requesting again by setting the ip's "previous access time" to the future
			//delta := time.Second - s.RateLimitDuration
			s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, rl.redirectPath})
			return true
		}
	} else {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, ""})
		return false
	}
}

func (s *Server) IPRateLimit_IsExpectingRedirect(ip string) bool {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		if rl.redirectPath != "" {
			return true
		}
	}
	return false
}
func (s *Server) IpRateLimit_GetExpectedRedirectPath(ip string) string {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		return rl.redirectPath
	}
	return ""
}

// When a redirection occurs, let server know to expect another request immediately following the previous one so that the rate-limiting can be bypassed
func (s *Server) IPRateLimit_ExpectRedirectPath(ip string, redirectPath string) {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{rl.prevTime, redirectPath})
	}
}
func (s *Server) IPRateLimit_ClearRedirectPath(ip string) {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{rl.prevTime, ""})
	}
}

// Removes all IPs that are passed the rate-limit duration.
// There's no need to call this too often, but do not wait so long that the ip list becomes too big.
func (s *Server) CleanupRateLimiting() {
	fmt.Printf("[Rate-Limit:%s:%s] Cleaning up rate limiting map\n", s.BindAddress, s.Port)
	currentTime := time.Now().UTC()
	items := s.ipRateLimitList.IterBuffered()
	for item := range items {
		prevTime := item.Val.prevTime
		redirectPath := item.Val.redirectPath
		if currentTime.After(prevTime.Add(time.Duration(s.RateLimit)*time.Millisecond)) && redirectPath == "" {
			s.ipRateLimitList.Remove(item.Key)
		}
	}
}

// Starts a goroutine that cleans up the ipRateLimitList map every 6 hours.
func (s *Server) startRateLimitingCleanerLoop() {
	timer := time.NewTicker(time.Hour * 6)
	go func() {
		for {
			<-timer.C
			s.CleanupRateLimiting()
		}
	}()
}

func (server *Server) Start() {
	// Load Host Certificates
	for k := range server.Hosts {
		server.Hosts[k].Setup()
		server.Hosts[k].loadCertificate()
	}

	fmt.Printf("[Listener:%s:%s] Starting with %d max concurrent connections.\n", server.BindAddress, server.Port, server.MaxConcurrentConnections)

	tlsConfig := &tls.Config{
		MinVersion: tls.VersionTLS12,
		ClientAuth: tls.RequestClientCert,
		NextProtos: []string{"scroll"},
	}
	tlsConfig.Certificates = make([]tls.Certificate, 1)

	// Default cert is the first certificate found for this port listener.
	var defaultCert tls.Certificate

	fmt.Printf("Default hostname: %s\n", server.DefaultHostname)
	if v, has := server.Hosts[server.DefaultHostname]; has {
		defaultCert = tls.Certificate{Certificate: [][]byte{v.Certificate.Certificate.Raw}, PrivateKey: v.Certificate.PrivateKey, Leaf: v.Certificate.Certificate}
	}
	tlsConfig.Certificates[0] = defaultCert

	// SNI Handling
	strictSni := true
	tlsConfig.GetCertificate = func(chi *tls.ClientHelloInfo) (*tls.Certificate, error) {
		//fmt.Printf("Getting cert for requested hostname: %s\n", chi.ServerName)
		if c, has := server.Hosts[chi.ServerName]; has {
			return &tls.Certificate{Certificate: [][]byte{c.Certificate.Certificate.Raw}, PrivateKey: c.Certificate.PrivateKey, Leaf: c.Certificate.Certificate}, nil
		} else if strictSni {
			return nil, fmt.Errorf("strict SNI enabled - No certificate found for domain: %q, closing connection", chi.ServerName)
		} else {
			return &defaultCert, nil
		}
	}

	// Support logging TLS keys for debugging
	keylogfile := os.Getenv("SSLKEYLOGFILE")
	if keylogfile != "" {
		w, err := os.OpenFile(keylogfile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0660)
		if err == nil {
			tlsConfig.KeyLogWriter = w
			defer w.Close()
		}
	}

	// TODO: tcp4 and tcp6 options, also the bind address for ipv6 is different (especially if trying to listen on all interfaces with ipv6; should be something like `[::1]`?)
	listener, listenErr := net.Listen("tcp", server.BindAddress+":"+server.Port)
	if listenErr != nil {
		panic(listenErr)
	}

	keepAliveListener := &tcpKeepAliveListener{listener.(*net.TCPListener)}
	tlsListener := tls.NewListener(keepAliveListener, tlsConfig)
	defer tlsListener.Close()

	// Loop to accept incoming connections
	//var bufpool sync.Pool
	//bufio.NewReadWriter(nil, nil)
	/*bufpool.New = func() interface{} {
		return bufio.NewReadWriter(bufio.NewReaderSize(nil, 1024), bufio.NewWriterSize(nil, 1))
	}*/
	var connChan chan *tls.Conn
	if server.MaxConcurrentConnections == -1 {
		connChan = make(chan *tls.Conn, 4000)
	} else {
		connChan = make(chan *tls.Conn, server.MaxConcurrentConnections*2)
	}

	var sem *semaphore.Weighted
	if server.MaxConcurrentConnections == -1 { // No Limit - semaphores released almost as soon as they are acquired
		sem = semaphore.NewWeighted(int64(runtime.GOMAXPROCS(0)) * 4)
	} else {
		sem = semaphore.NewWeighted(int64(server.MaxConcurrentConnections))
	}

	acceptLoop := func() {
		for {
			// Wait until there's an open space for the connection
			sem.Acquire(context.Background(), 1)
			conn, err := tlsListener.Accept() // NOTE: Blocking (unless listener is closed)
			if errors.Is(err, net.ErrClosed) {
				sem.Release(1)
				break
			} else if err != nil {
				fmt.Printf("accept error: %v\n", err)
				sem.Release(1)
				continue
			}

			// Check IP Blocking stuff
			ip, _, _ := net.SplitHostPort(conn.RemoteAddr().String())
			if server.IsIPBlocked(ip) {
				conn.Close()
				sem.Release(1)
				continue
			}

			// If not expecting redirect, do rate limiting here. Otherwise, wait to do rate limiting until we have read the header of the request (which occurs in the goroutine for the connection)
			/*if !s.IPRateLimit_IsExpectingRedirect(ip) {
				if s.IsIPRateLimited(ip) {
					// Rate limited, return a 44 slow down error
					conn.Write([]byte("44 1\r\n"))
					conn.Close()
					sem.Release(1)
					continue
				}
			}*/

			tlsConn, isTLS := conn.(*tls.Conn)
			if !isTLS {
				fmt.Printf("[Listener:%s:%s] Expected tls connection, got non-tls connection.\n", server.BindAddress, server.Port)
				conn.Close()
				sem.Release(1)
				continue
			}

			// Send the connection to the main loop
			connChan <- tlsConn
		}
	}
	go acceptLoop()

mainloop:
	for {
		var netConn net.Conn
		ok := true
		received := 0
		hasConn := false

		// Try to get both
		var msg string
		select {
		case msg, ok = <-server.msg:
			received++
		default:
		}
		select {
		case netConn = <-connChan:
			received++
			hasConn = true
		default:
		}

		// Haven't received anything, block the loop until one is received
		if received == 0 {
			select {
			case msg, ok = <-server.msg:
			case netConn = <-connChan:
				hasConn = true
			}
		}
		// If need to shut down
		if !ok {
			fmt.Printf("[PortListener:%s:%s] Shutting down.\n", server.BindAddress, server.Port)
			//l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.Port, "Shutting down.")

			if hasConn {
				netConn.Close()
			}
			tlsListener.Close()
			break mainloop
		} else if !hasConn {
			// Handle server messages here
			switch msg {
			case "enter_maintenance":
				//l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.Port, "Entering maintenance mode.")
				server.maintenanceMode = true
			case "exit_maintenance":
				//l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.Port, "Exiting maintenance mode.")
				server.maintenanceMode = false
			}
		}

		// If no max number of concurrent connections specified, then release 1 from the semaphor before the goroutine starts
		if server.MaxConcurrentConnections == -1 {
			sem.Release(1)
		}

		//fmt.Printf("Negotiated protocol: %s\n", netConn.(*tls.Conn).ConnectionState().NegotiatedProtocol)

		if hasConn {
			go server.multiplex(netConn, sem, true)
		}
	}
}

// Multiplexes multiple protocols, finds the server for each, and sends the connection off to the proper server
func (server *Server) multiplex(conn net.Conn, sem *semaphore.Weighted, isTLS bool) {
	defer conn.Close()
	defer func() {
		if server.MaxConcurrentConnections != -1 {
			sem.Release(1)
		}
	}()

	defer func() {
		if r := recover(); r != nil {
			panicString := ""
			switch r := r.(type) {
			case string:
				panicString = r
			}
			fmt.Printf("[PortListener:%s:%s] panic: %s\n%s\n", server.BindAddress, server.Port, panicString, string(debug.Stack()))
			//l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.Port, "panic: %s\n%s\n", panicString, string(debug.Stack()))
		}
	}()

	// Check if TLS Connection, and get the TLS connection
	tlsConn := conn.(*tls.Conn)
	// Give 10 seconds max for the TLS handshake
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	err := tlsConn.HandshakeContext(ctx)
	cancel()
	if err != nil {
		fmt.Printf("TLS handshake error: %v\n", err)
		return
	}

	// Check Scroll, Gopher SSL, Gemini, Misfin, or Titan
	requestHeader, err := server.readTLSHeader(tlsConn)
	if err != nil {
		fmt.Printf("Error reading tls header: %s\n", err.Error())
		switch requestHeader.Protocol {
		case ProtocolType_Gemini, ProtocolType_Scroll:
			tlsConn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
		case ProtocolType_Titan:
			tlsConn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
		case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
			tlsConn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
		case ProtocolType_Gopher_SSL:
			tlsConn.Write([]byte(fmt.Sprintf("3%s.\t/\t%s\t%s\r\n", err.Error(), server.DefaultHostname, server.ServePort)))
		}
		return
	}

	if host, hasHost := server.Hosts[requestHeader.Hostname]; hasHost {
		if server.maintenanceMode {
			switch requestHeader.Protocol {
			case ProtocolType_Gemini, ProtocolType_Scroll:
				tlsConn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
			case ProtocolType_Titan:
				tlsConn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
			case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
				tlsConn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
			case ProtocolType_Gopher_SSL:
				tlsConn.Write([]byte(fmt.Sprintf("3Server in Maintenance Mode\t/\t%s\t%s\r\n", host.Hostname, server.Port)))
			}
			return
		}

		host.HandleRequest(tlsConn, requestHeader)
	} else {
		switch requestHeader.Protocol {
		case ProtocolType_Gemini, ProtocolType_Scroll:
			tlsConn.Write([]byte("53 Domain not served.\r\n"))
		case ProtocolType_Titan:
			tlsConn.Write([]byte("53 Domain not served.\r\n"))
		case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
			tlsConn.Write([]byte("53 Domain not served.\r\n"))
		case ProtocolType_Gopher_SSL:
			tlsConn.Write([]byte(fmt.Sprintf("3%s.\t/\t%s\t%s\r\n", "Domain not served.", server.DefaultHostname, server.ServePort)))
		}
		return
	}
}

// Read header for Gopher SSL, Gemini, Misfin, and Titan
// Returns protocol, request header, and error
func (server *Server) readTLSHeader(conn *tls.Conn) (RequestHeader, error) {
	tlsState := conn.ConnectionState()

	// Read the request header
	maxHeaderSize := 2048 - 2
	var request []byte = make([]byte, 0, maxHeaderSize)
	var requestHeaderOverflow bool

	// Read until we hit a \r\n or a space. Gemini and Titan don't use spaces in their request headers, but Misfin(B) and Scroll do.
	delim := []byte("\r\n")
	delim_misfinB := []byte(" ")
	buf := make([]byte, 1)
	for {
		n, err := conn.Read(buf)
		if err == io.EOF && n <= 0 {
			return RequestHeader{}, err
		} else if err != nil && err != io.EOF {
			return RequestHeader{}, err
		}

		request = append(request, buf...)
		// Parse request string until a CRLF (for gemini and titan) or a space (for misfin). In misfin, everything after the space is the metadata and message body, ending with another CRLF
		if bytes.HasSuffix(request, delim) {
			request = request[:len(request)-len(delim)]
			break
		} else if bytes.HasSuffix(request, delim_misfinB) {
			request = request[:len(request)-len(delim_misfinB)]
			break
		}

		// Detect HTTP Verbs and instantly reject the request
		if bytes.HasPrefix(request, []byte("GET")) || bytes.HasPrefix(request, []byte("HEAD")) || bytes.HasPrefix(request, []byte("POST")) || bytes.HasPrefix(request, []byte("PUT")) || bytes.HasPrefix(request, []byte("DELETE")) || bytes.HasPrefix(request, []byte("CONNECT")) || bytes.HasPrefix(request, []byte("OPTIONS")) || bytes.HasPrefix(request, []byte("TRACE")) || bytes.HasPrefix(request, []byte("PATCH")) {
			return RequestHeader{}, fmt.Errorf("http not supported")
		}

		if len(request) > maxHeaderSize {
			requestHeaderOverflow = true
			break
		}
	}

	// Check for a tab, which will determine weather this request header is a Misfin(C) request or a Misfin(B) request.
	// Misfin C Format: misfin://<MAILBOX>@<HOSTNAME><TAB><CONTENT-LENGTH><CRLF><MESSAGE>
	// Misfin B Format: misfin://<MAILBOX>@<HOSTNAME><SPACE><MESSAGE><CRLF>
	// NOTE: This won't mess up Titan or Gemini parsing, as their request headers dont have tabs in them.
	requestHeader, contentLengthStr, isMisfinC := strings.Cut(string(request), "\t")

	// Get the URL
	URL, err := url.Parse(requestHeader)
	if err != nil {
		return RequestHeader{}, fmt.Errorf("invalid URL")
	}

	// gopher requests don't use a scheme or hostname
	if URL.Scheme == "" {
		// Try to get a protocol with ALPN
		if tlsState.NegotiatedProtocol != "" {
			URL.Scheme = tlsState.NegotiatedProtocol
		} else {
			// Otherwise default to scroll scheme
			URL.Scheme = "scroll"
		}
	}

	var resultHeader RequestHeader
	resultHeader.Hostname = URL.Hostname()
	if resultHeader.Hostname == "" { // If no hostname in URL, get hostname from SNI (if available)
		resultHeader.Hostname = tlsState.ServerName
		// Otherwise, use the defaultServer
		if resultHeader.Hostname == "" {
			resultHeader.Hostname = server.DefaultHostname
		}
	}
	resultHeader.Protocol = ProtocolType_Gemini
	switch URL.Scheme {
	case "gemini":
		resultHeader.Protocol = ProtocolType_Gemini
		resultHeader.Request = requestHeader
	case "gophers", "gopher":
		resultHeader.Protocol = ProtocolType_Gopher_SSL
		resultHeader.Request = requestHeader
	case "scroll":
		resultHeader.Protocol = ProtocolType_Scroll

		// Since we stopped at a space, get the rest of the header data, stopping at \r\n
		var restOfRequest []byte
		for {
			n, err := conn.Read(buf)
			if err == io.EOF && n <= 0 {
				return RequestHeader{}, err
			} else if err != nil && err != io.EOF {
				return RequestHeader{}, err
			}
			restOfRequest = append(restOfRequest, buf...)
			if bytes.HasSuffix(restOfRequest, []byte("\r\n")) {
				restOfRequest = restOfRequest[:len(restOfRequest)-len("\r\n")]
				break
			}

			if len(restOfRequest) > maxHeaderSize-len(requestHeader) {
				requestHeaderOverflow = true
				break
			}
		}
		resultHeader.Request = requestHeader
		if strings.HasPrefix(string(restOfRequest), "+") {
			resultHeader.MetadataRequest = true
			restOfRequest = restOfRequest[len("+"):]
		}
		parts := strings.Split(string(restOfRequest), ",")
		for _, p := range parts {
			resultHeader.Languages = append(resultHeader.Languages, strings.TrimSpace(p))
		}
	case "misfin":
		resultHeader.Request = requestHeader
		if isMisfinC {
			resultHeader.Protocol = ProtocolType_Misfin_C
			var parseErr error
			resultHeader.ContentLength, parseErr = strconv.ParseInt(contentLengthStr, 10, 64)
			if parseErr != nil {
				fmt.Printf("error reading request\n")
				responseBadURL := "59 Request invalid.\r\n"
				conn.Write([]byte(responseBadURL))
				return resultHeader, parseErr
			}
			if resultHeader.ContentLength > 16384 {
				fmt.Printf("request length too long\n")
				responseLengthTooLong := "59 Request length too long. Total metadata+message length cannot be more than 16KB for Misfin(C), or 2KB for Misfin(AB).\r\n"
				conn.Write([]byte(responseLengthTooLong))
				return resultHeader, fmt.Errorf("content length too long")
			}
		} else {
			resultHeader.Protocol = ProtocolType_Misfin_B
		}
	case "titan":
		resultHeader.Protocol = ProtocolType_Titan
		parts := strings.Split(requestHeader, ";")
		resultHeader.Request = parts[0]
		header_params := parts[1:]

		// Get header params
		foundSize := false
		for _, p := range header_params {
			parts := strings.SplitN(p, "=", 2)
			key := parts[0]
			value := parts[1]
			if key == "token" {
				resultHeader.Token = value
			} else if key == "mime" {
				resultHeader.Mime = value
			} else if key == "size" {
				foundSize = true
				var err error
				resultHeader.ContentLength, err = strconv.ParseInt(value, 10, 64)
				if err != nil {
					fmt.Printf("Titan Error: Size parameter is not an integer; %s; %#v\n", resultHeader.Request, header_params)
					responseBadRequest_SizeParamNotInt := "59 Size parameter is not an integer.\r\n"
					conn.Write([]byte(responseBadRequest_SizeParamNotInt))
					return resultHeader, err
				}
			}
		}

		// Default mime to "text/gemini"
		if resultHeader.Mime == "" {
			resultHeader.Mime = "text/gemini"
		}

		if !foundSize {
			// Error: Size is required
			fmt.Printf("Titan Error: No Size parameter given; %s; %#v\n", requestHeader, header_params)
			responseBadRequest_NoSizeParam := "59 No Size parameter given.\r\n"
			conn.Write([]byte(responseBadRequest_NoSizeParam))
			return resultHeader, fmt.Errorf("no size parameter given")
		}
	default:
		resultHeader.Protocol = ProtocolType_Unknown
	}

	// Check overflows
	if requestHeaderOverflow {
		fmt.Printf("request length too long\n")
		responseLengthTooLong := "59 Request length too long. Total length cannot be more than 1KB for Misfin(C)'s request header, 1KB for Gemini, and 2KB for Misfin(AB)'s request+metadata+message.\r\n"
		conn.Write([]byte(responseLengthTooLong))
		return resultHeader, fmt.Errorf("request length too long")
	}

	if len(tlsState.PeerCertificates) > 0 {
		resultHeader.UserCert = tlsState.PeerCertificates[0]
	}

	return resultHeader, nil
}

type RequestHeader struct {
	Protocol        ProtocolType
	Request         string            // The Selector/Path or URL that is requested
	Hostname        string            // Used for Scroll, Gemini, Misfin, and Titan
	ContentLength   int64             // Used for Titan, Misfin, and Spartan. If Titan protocol and 0, it's a titan delete request. If Spartan and 0, no upload data is expected.
	Token           string            // Used for Titan
	Mime            string            // Used for Titan
	UserCert        *x509.Certificate // For TLS protocols
	MetadataRequest bool              // Used for Scroll protocol
	Languages       []string          // Used for Scroll protocol
}

// tcpKeepAliveListener sets TCP keep-alive timeouts on accepted
// connections. It's used by Run so dead TCP connections (e.g.
// closing laptop mid-download) eventually go away.
type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	if c, err = ln.AcceptTCP(); err != nil {
		return
	} else if err = c.(*net.TCPConn).SetKeepAlive(true); err != nil {
		return
	}
	// Ignore error from setting the KeepAlivePeriod as some systems, such as
	// OpenBSD, do not support setting TCP_USER_TIMEOUT on IPPROTO_TCP
	_ = c.(*net.TCPConn).SetKeepAlivePeriod(3 * time.Minute)

	return
}
