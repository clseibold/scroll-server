package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// Config:
// * Server Directory - contains config and `public` sub-dir for serving
// * Server cert file
// * Bind Address, Port, Hostnames (and ServePort)
// * Rate-limit
// * Command to generate new certificate

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Subcommands:\n")
		fmt.Printf("  %-10s %s\n", "start", "Starts the server given the server directory.")
		fmt.Printf("  %-10s %s\n", "gen", "Generates a new certificate.")
		fmt.Print("\n")
		os.Exit(1)
	}

	cmd := os.Args[1]
	args := os.Args[2:]
	switch cmd {
	case "start":
		startServer(args)
	case "gen":
		generateCertificate(args, nil)
	}
}

func startServer(args []string) {
	if len(args) < 1 {
		fmt.Printf("You must pass in the root server directory to start the server from.\n")
		os.Exit(1)
	}

	// Check if server directory exists
	serverDirectory, _ := filepath.Abs(args[0])
	info, err := os.Stat(serverDirectory)
	if err != nil {
		err := os.MkdirAll(serverDirectory, 0700)
		if err != nil {
			fmt.Printf("Error: Couldn't create server directory.\n")
			os.Exit(1)
		}
	} else if !info.IsDir() {
		fmt.Printf("Error: Filepath given is not a directory.\n")
		os.Exit(1)
	}

	server := NewServer(serverDirectory)

	// Check if config file exists. If not, then ask for config info.
	configFilename := filepath.Join(serverDirectory, "scrollserver.conf")
	_, err = os.Stat(configFilename)
	if os.IsNotExist(err) {
		// Ask for config details: bind-address, port, serve-port, rate-limit, and default hostname
		reader := bufio.NewReader(os.Stdin)

		fmt.Print("Bind-address ('0.0.0.0'): ")
		bind_address, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("Error: Couldn't read input.\n")
			os.Exit(1)
		}
		bind_address = strings.TrimSpace(bind_address)
		if bind_address == "" {
			bind_address = "0.0.0.0"
		}
		server.BindAddress = bind_address

		fmt.Print("Port ('5699'): ")
		port, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("Error: Couldn't read input.\n")
			os.Exit(1)
		}
		port = strings.TrimSpace(port)
		if port == "" {
			port = "5699"
		}
		server.Port = port

		fmt.Printf("Serve-Port ('%s'): ", port)
		serve_port, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("Error: Couldn't read input.\n")
			os.Exit(1)
		}
		serve_port = strings.TrimSpace(serve_port)
		if serve_port == "" {
			serve_port = port
		}
		server.ServePort = serve_port

		for {
			fmt.Print("Rate-limit in milliseconds ('225'): ")
			rate_limit_str, err := reader.ReadString('\n')
			if err != nil {
				fmt.Printf("Error: Couldn't read input.\n")
				os.Exit(1)
			}
			rate_limit_str = strings.TrimSpace(rate_limit_str)
			if rate_limit_str == "" {
				rate_limit_str = "225"
			}
			server.RateLimit, err = strconv.Atoi(rate_limit_str)
			if err != nil {
				fmt.Printf("Error: Not an integer.\n")
				continue
			}
			break
		}

		fmt.Printf("Default language in BCP47 ('en'): ")
		default_language, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("Error: Couldn't read input.\n")
			os.Exit(1)
		}
		default_language = strings.TrimSpace(default_language)
		if default_language == "" {
			default_language = "en"
		}
		server.DefaultLanguage = default_language

		fmt.Print("Default hostname of server: ")
		default_hostname, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("Error: Couldn't read input.\n")
			os.Exit(1)
		}
		default_hostname = strings.TrimSpace(default_hostname)
		if server.Hosts == nil {
			server.Hosts = make(map[string]*Host, 1)
		}
		host := NewHost(server)
		host.Hostname = default_hostname
		host.Default = true

		// Check if certificate file already exists
		host.CertificateFile = filepath.Join(server.ServerDirectory, default_hostname+".pem")
		_, err = os.Stat(host.CertificateFile)
		if os.IsNotExist(err) {
			fmt.Printf("Generating certificate for default hostname:\n")
			host.CertificateFile = generateCertificate([]string{server.ServerDirectory}, []string{default_hostname})
		}

		if host.Directory == "" {
			host.Directory = filepath.Join(serverDirectory, host.Hostname)
			os.Mkdir(host.Directory, 0700)
		}
		server.DefaultHostname = default_hostname
		server.Hosts[default_hostname] = host
		server.saveConfig()
	} else {
		// Load config file
		server.loadConfig()
	}

	server.Start()
}

// Returns filepath of certificate
func generateCertificate(args []string, hostnames []string) string {
	if len(args) < 1 {
		fmt.Printf("You must pass in the root server directory to store the new certificate.\n")
		os.Exit(1)
	}

	// Make sure directory exists
	serverDirectory := filepath.Clean(args[0])
	err := os.MkdirAll(serverDirectory, 0700)
	if err != nil {
		fmt.Printf("Error: Couldn't make server directory; %s\n", err.Error())
		os.Exit(1)
	}

	// Ask for Certificate Information
	reader := bufio.NewReader(os.Stdin)
	if len(hostnames) == 0 {
		fmt.Print("Hostnames (comma-delimited): ")
		hostnames_str, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("Error: Couldn't read input.\n")
			os.Exit(1)
		}
		hostnames = strings.Split(strings.TrimSpace(hostnames_str), ",")
		for i, hostname := range hostnames {
			hostnames[i] = strings.TrimSpace(hostname)
		}
	}

	fmt.Print("Country ('US'): ")
	country, err := reader.ReadString('\n')
	if err != nil {
		fmt.Printf("Error: Couldn't read input.\n")
		os.Exit(1)
	}
	country = strings.TrimSpace(country)
	if country == "" {
		country = "US"
	}

	fmt.Print("Organization: ")
	organization, err := reader.ReadString('\n')
	if err != nil {
		fmt.Printf("Error: Couldn't read input.\n")
		os.Exit(1)
	}
	organization = strings.TrimSpace(organization)

	pemBuffer := &bytes.Buffer{}
	_, err = generateSelfSignedCertAndPrivateKey(hostnames[0], []string{country}, []string{organization}, []string{"IT"}, hostnames, pemBuffer)
	if err != nil {
		fmt.Printf("Error: Couldn't generate certificate; %s\n", err.Error())
		os.Exit(1)
	}

	filename := filepath.Join(serverDirectory, hostnames[0]+".pem")

	// Detect if cert already exists and ask to overwrite.
	_, err = os.Stat(filename)
	if os.IsNotExist(err) {
		// Write the file
		os.WriteFile(filename, pemBuffer.Bytes(), 0600)
		fmt.Printf("Certificate file written at '%s'.\n", filename)
		return filename
	} else if err != nil {
		fmt.Printf("Error: Couldn't stat.\n")
		os.Exit(1)
	} else {
		// File already exists, ask to overwrite
		fmt.Printf("Certificate file already exists at '%s'. Overwrite? [Y/N]", filename)
		YN, err := reader.ReadString('\n')
		if err != nil {
			fmt.Printf("Error: Couldn't read input.\n")
			os.Exit(1)
		}
		YN = strings.ToLower(YN)
		if YN == "y" || YN == "yes" {
			os.WriteFile(filename, pemBuffer.Bytes(), 0600)
			fmt.Print("Certificate file overwritten.\n")
			return filename
		} else {
			fmt.Printf("Cert file not written.\n")
			os.Exit(1)
		}
	}

	return ""
}
