module gitlab.com/clseibold/scroll-server

go 1.22.0

require (
	github.com/dhowden/tag v0.0.0-20240417053706-3d75831295e8 // indirect
	github.com/djherbis/times v1.6.0 // indirect
	github.com/eidolon/wordwrap v0.0.0-20161011182207-e0f54129b8bb // indirect
	github.com/gabriel-vasile/mimetype v1.4.3 // indirect
	github.com/gammazero/deque v0.2.1 // indirect
	github.com/orcaman/concurrent-map/v2 v2.0.1 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
