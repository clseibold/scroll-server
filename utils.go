package main

import "strings"

// CutAny slices s around any Unicode code point from chars,
// returning the text before and after it. The found result
// reports whether any Unicode code point was appears in s.
// If it does not appear in s, CutAny returns s, "", false.
func CutAny(s string, chars string) (before string, after string, found bool) {
	if index := strings.IndexAny(s, chars); index >= 0 {
		return s[:index], strings.TrimLeft(s[index:], chars), true
	}
	return s, "", false
}
