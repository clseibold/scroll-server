# Scroll-Server

This is a static-file server for the Scroll Protocol. It is intended to be a simplified version of the server code from my [SIS project](https://gitlab.com/clseibold/smallnetinformationservices). For more advanced options, use SIS.

You can get a scroll terminal [client here](https://gitlab.com/clseibold/scroll-term), and a [graphical browser here](https://gitlab.com/clseibold/profectus). The [Scroll Protocol homepage](scroll://scrollprotocol.us.to/) has more info on the Scroll Protocol.

A couple of features are still being worked on:

- Internationalization
  - provide multiple files in different languages at the same address/route
  - provide different serving directories for different languages
  - files not found in one language serving directory should be served from the default language's directory instead
- Specify CGI routes in config file
- How requested language is handled with input requests.
  - Language of input should be interpreted as the language that was matched. The language list string in the request shouldn't change between the input response and the input submission request.

## Install or build

You can install via golang:

```
go install gitlab.com/clseibold/scroll-server@latest
```

Or build locally:

```
go build .
```

## Setup Server Config

```
scroll-server start [server_directory_path]
```

On first run of the start command, you will be asked for configuration options, and you will be asked to setup the default virtual host. It will also generate the certificate for you if necessary.

## Generate Certificate

```
scroll-server gen [server_directory_path]
```

It will interactively ask for options. The cert file is saved in the server directory using the provided hostname as its name, suffixed with ".pem".

## Start the Server

```
scroll-server start [server_directory_path]
```

## Serving Directory

The directory being served is always `[server_directory_path]/[hostname]`. In other words, it is a directory named with the hostname under the root server directory.

## Abstracts and Author Metadata

All files can have abstracts by placing a scrolltext file next to it with the same name and extension, suffixed with ".abstract". For example, an abstract to a "song.mp3" file should be named "song.mp3.abstract" and contain scrolltext with a level-1 heading for the file's title.

The abstract for the index.scroll file in a directory may be named either `index.scroll.abstract` or just `.abstract`. The latter is preferred when both exist.

The Publish and Modification dates comes from the filesystem's info for that file. The Author metadata may be specified by the abstract file before the level-1 heading by using the format:

```
Author: Author Name Here
# Level-1 Heading Title

Rest of abstract file.
```

You can also override the publish, modification dates, UDC classification, and the BCP47 language string, by placing them in the abstract file similarly to the author, like so:

```
Author: Author Name
Publish-Date: 2024-04-05T04:00:00Z
Modification-Date: 2024-04-05T04:00:00Z
Language: en-US
Udc-class: 0
# Level-1 Heading Title

Rest of abstract file.
```

Files that don't declare a language will use the default language of the server, which is specified in the config file and configured when you first start the server.

You can also add these fields to the beginning of scrolltext files in case you don't want to create a separate abstract for that file.

Audio files will automatically read the tag data and use that information to constrct the metadata and abstract, if an abstract does not already exist on the filesystem for the file.

### UDC Classification Strings

Instead of putting the UDC class integer, these strings are provided to give memorable names to different content types in each UDC class:

- class 0: "knowledge", "docs", "documentation", "data", "general science", "general knowledge", "reference", "computer science", "news", "software repository", "software repo", "computer technology", "howto", "tutorial", "devlog"
- class 1: "philosophy", "psychology"
- class 2: "religion", "theology", "scripture"
- class 3: "social science", "social sciences", "law", "politics", "gender studies", "sociology", "LGBTQ+", "military", "military affairs", "education"
- class 5: "mathematics", "math", "maths", "natural science"
- class 6: "applied science", "applied sciences", "technology", "tech", "medicine", "health", "engineering", "business", "accountancy", "accounting"
- class 7: "art", "arts", "entertainment", "music", "gaming", "gaming video", "gaming videos", "sport", "sports", "fictional movies", "fictional movie", "fictional animation", "fictional animations", "fictional anime", "fictional animes", "fashion", "beauty", "culinary art", "culinary arts", "recreation", "fitness", "drama", "performance", "performance art", "performance arts"
- class 8: "linguistics", "language", "langauges", "literature", "literary criticism", "biblical criticism", "memoir", "personal log", "personal blog", "book review", "movie review", "video review", "music review"
- class 9: "history", "geography", "biography", "autobiography"
- class 4: "unclassed", "unclassed software", "general software", "general aggregator", "general directory", "general index", "menu"

## Multi-Language Handling

Currently the server doesn't support serving the same file (at the same link address) in different languages, but this will be coming in the near future.

## Virtual Hosting

The configuration will allow you to setup virtual hosts with certificates for each. They are each served in their own directories under the server root directory.

The server will use SNI to handle knowing which hostname to serve up.
